-- MySQL dump 10.15  Distrib 10.0.31-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: PSS_DEV
-- ------------------------------------------------------
-- Server version	10.0.31-MariaDB-0ubuntu0.16.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `competition`
--

DROP TABLE IF EXISTS `competition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `competition` (
  `competition_id` int(11) NOT NULL,
  `competition_name` varchar(45) NOT NULL,
  `competition_season` varchar(45) NOT NULL,
  PRIMARY KEY (`competition_name`,`competition_season`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `event`
--

DROP TABLE IF EXISTS `event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event` (
  `event_date` varchar(45) NOT NULL,
  `event_goals_home` int(11) DEFAULT NULL,
  `event_goals_away` int(11) DEFAULT NULL,
  `event_goals_home_ht` int(11) DEFAULT NULL,
  `event_goals_away_ht` int(11) DEFAULT NULL,
  `event_competition_name` varchar(45) DEFAULT NULL,
  `event_team_home_name` varchar(45) NOT NULL,
  `event_team_away_name` varchar(45) NOT NULL,
  `event_competition_season` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`event_date`,`event_team_away_name`,`event_team_home_name`),
  KEY `fk_event_team_home_1_idx` (`event_team_home_name`),
  KEY `fk_event_team_away_1_idx` (`event_team_away_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `statistic`
--

DROP TABLE IF EXISTS `statistic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statistic` (
  `statistic_goals_home` int(11) DEFAULT NULL,
  `statistic_goals_away` int(11) DEFAULT NULL,
  `statistic_wins_home` int(11) DEFAULT NULL,
  `statistic_wins_away` int(11) DEFAULT NULL,
  `statistic_losses_home` int(11) DEFAULT NULL,
  `statistic_losses_away` int(11) DEFAULT NULL,
  `statistic_draws_home` int(11) DEFAULT NULL,
  `statistic_draws_away` int(11) DEFAULT NULL,
  `statistic_team_name` varchar(45) NOT NULL,
  `statistic_competition_id` int(11) NOT NULL,
  `statistic_competition_name` varchar(45) DEFAULT NULL,
  `statistic_competition_season` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`statistic_competition_id`,`statistic_team_name`),
  KEY `fk_statistics_competition_1_idx` (`statistic_competition_id`),
  KEY `fk_statistics_team_1_idx` (`statistic_team_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `team`
--

DROP TABLE IF EXISTS `team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team` (
  `team_name` varchar(45) NOT NULL,
  `team_short_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`team_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-13 10:05:36
