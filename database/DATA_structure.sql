-- MySQL dump 10.15  Distrib 10.0.31-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: DATA
-- ------------------------------------------------------
-- Server version	10.0.31-MariaDB-0ubuntu0.16.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Country`
--

DROP TABLE IF EXISTS `Country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=24559 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Event`
--

DROP TABLE IF EXISTS `Event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) DEFAULT NULL,
  `league_id` int(11) DEFAULT NULL,
  `season` varchar(100) DEFAULT NULL,
  `stage` int(11) DEFAULT NULL,
  `date` varchar(100) DEFAULT NULL,
  `match_api_id` int(11) DEFAULT NULL,
  `home_team_api_id` int(11) DEFAULT NULL,
  `away_team_api_id` int(11) DEFAULT NULL,
  `home_team_goal` int(11) DEFAULT NULL,
  `away_team_goal` int(11) DEFAULT NULL,
  `home_player_X1` int(11) DEFAULT NULL,
  `home_player_X2` int(11) DEFAULT NULL,
  `home_player_X3` int(11) DEFAULT NULL,
  `home_player_X4` int(11) DEFAULT NULL,
  `home_player_X5` int(11) DEFAULT NULL,
  `home_player_X6` int(11) DEFAULT NULL,
  `home_player_X7` int(11) DEFAULT NULL,
  `home_player_X8` int(11) DEFAULT NULL,
  `home_player_X9` int(11) DEFAULT NULL,
  `home_player_X10` int(11) DEFAULT NULL,
  `home_player_X11` int(11) DEFAULT NULL,
  `away_player_X1` int(11) DEFAULT NULL,
  `away_player_X2` int(11) DEFAULT NULL,
  `away_player_X3` int(11) DEFAULT NULL,
  `away_player_X4` int(11) DEFAULT NULL,
  `away_player_X5` int(11) DEFAULT NULL,
  `away_player_X6` int(11) DEFAULT NULL,
  `away_player_X7` int(11) DEFAULT NULL,
  `away_player_X8` int(11) DEFAULT NULL,
  `away_player_X9` int(11) DEFAULT NULL,
  `away_player_X10` int(11) DEFAULT NULL,
  `away_player_X11` int(11) DEFAULT NULL,
  `home_player_Y1` int(11) DEFAULT NULL,
  `home_player_Y2` int(11) DEFAULT NULL,
  `home_player_Y3` int(11) DEFAULT NULL,
  `home_player_Y4` int(11) DEFAULT NULL,
  `home_player_Y5` int(11) DEFAULT NULL,
  `home_player_Y6` int(11) DEFAULT NULL,
  `home_player_Y7` int(11) DEFAULT NULL,
  `home_player_Y8` int(11) DEFAULT NULL,
  `home_player_Y9` int(11) DEFAULT NULL,
  `home_player_Y10` int(11) DEFAULT NULL,
  `home_player_Y11` int(11) DEFAULT NULL,
  `away_player_Y1` int(11) DEFAULT NULL,
  `away_player_Y2` int(11) DEFAULT NULL,
  `away_player_Y3` int(11) DEFAULT NULL,
  `away_player_Y4` int(11) DEFAULT NULL,
  `away_player_Y5` int(11) DEFAULT NULL,
  `away_player_Y6` int(11) DEFAULT NULL,
  `away_player_Y7` int(11) DEFAULT NULL,
  `away_player_Y8` int(11) DEFAULT NULL,
  `away_player_Y9` int(11) DEFAULT NULL,
  `away_player_Y10` int(11) DEFAULT NULL,
  `away_player_Y11` int(11) DEFAULT NULL,
  `home_player_1` int(11) DEFAULT NULL,
  `home_player_2` int(11) DEFAULT NULL,
  `home_player_3` int(11) DEFAULT NULL,
  `home_player_4` int(11) DEFAULT NULL,
  `home_player_5` int(11) DEFAULT NULL,
  `home_player_6` int(11) DEFAULT NULL,
  `home_player_7` int(11) DEFAULT NULL,
  `home_player_8` int(11) DEFAULT NULL,
  `home_player_9` int(11) DEFAULT NULL,
  `home_player_10` int(11) DEFAULT NULL,
  `home_player_11` int(11) DEFAULT NULL,
  `away_player_1` int(11) DEFAULT NULL,
  `away_player_2` int(11) DEFAULT NULL,
  `away_player_3` int(11) DEFAULT NULL,
  `away_player_4` int(11) DEFAULT NULL,
  `away_player_5` int(11) DEFAULT NULL,
  `away_player_6` int(11) DEFAULT NULL,
  `away_player_7` int(11) DEFAULT NULL,
  `away_player_8` int(11) DEFAULT NULL,
  `away_player_9` int(11) DEFAULT NULL,
  `away_player_10` int(11) DEFAULT NULL,
  `away_player_11` int(11) DEFAULT NULL,
  `goal` varchar(100) DEFAULT NULL,
  `shoton` varchar(100) DEFAULT NULL,
  `shotoff` varchar(100) DEFAULT NULL,
  `foulcommit` varchar(100) DEFAULT NULL,
  `card` varchar(100) DEFAULT NULL,
  `cross` varchar(100) DEFAULT NULL,
  `corner` varchar(100) DEFAULT NULL,
  `possession` varchar(100) DEFAULT NULL,
  `B365H` decimal(10,0) DEFAULT NULL,
  `B365D` decimal(10,0) DEFAULT NULL,
  `B365A` decimal(10,0) DEFAULT NULL,
  `BWH` decimal(10,0) DEFAULT NULL,
  `BWD` decimal(10,0) DEFAULT NULL,
  `BWA` decimal(10,0) DEFAULT NULL,
  `IWH` decimal(10,0) DEFAULT NULL,
  `IWD` decimal(10,0) DEFAULT NULL,
  `IWA` decimal(10,0) DEFAULT NULL,
  `LBH` decimal(10,0) DEFAULT NULL,
  `LBD` decimal(10,0) DEFAULT NULL,
  `LBA` decimal(10,0) DEFAULT NULL,
  `PSH` decimal(10,0) DEFAULT NULL,
  `PSD` decimal(10,0) DEFAULT NULL,
  `PSA` decimal(10,0) DEFAULT NULL,
  `WHH` decimal(10,0) DEFAULT NULL,
  `WHD` decimal(10,0) DEFAULT NULL,
  `WHA` decimal(10,0) DEFAULT NULL,
  `SJH` decimal(10,0) DEFAULT NULL,
  `SJD` decimal(10,0) DEFAULT NULL,
  `SJA` decimal(10,0) DEFAULT NULL,
  `VCH` decimal(10,0) DEFAULT NULL,
  `VCD` decimal(10,0) DEFAULT NULL,
  `VCA` decimal(10,0) DEFAULT NULL,
  `GBH` decimal(10,0) DEFAULT NULL,
  `GBD` decimal(10,0) DEFAULT NULL,
  `GBA` decimal(10,0) DEFAULT NULL,
  `BSH` decimal(10,0) DEFAULT NULL,
  `BSD` decimal(10,0) DEFAULT NULL,
  `BSA` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `match_api_id` (`match_api_id`),
  KEY `country_id` (`country_id`),
  KEY `league_id` (`league_id`),
  KEY `home_team_api_id` (`home_team_api_id`),
  KEY `away_team_api_id` (`away_team_api_id`),
  KEY `home_player_1` (`home_player_1`),
  KEY `home_player_2` (`home_player_2`),
  KEY `home_player_3` (`home_player_3`),
  KEY `home_player_4` (`home_player_4`),
  KEY `home_player_5` (`home_player_5`),
  KEY `home_player_6` (`home_player_6`),
  KEY `home_player_7` (`home_player_7`),
  KEY `home_player_8` (`home_player_8`),
  KEY `home_player_9` (`home_player_9`),
  KEY `home_player_10` (`home_player_10`),
  KEY `home_player_11` (`home_player_11`),
  KEY `away_player_1` (`away_player_1`),
  KEY `away_player_2` (`away_player_2`),
  KEY `away_player_3` (`away_player_3`),
  KEY `away_player_4` (`away_player_4`),
  KEY `away_player_5` (`away_player_5`),
  KEY `away_player_6` (`away_player_6`),
  KEY `away_player_7` (`away_player_7`),
  KEY `away_player_8` (`away_player_8`),
  KEY `away_player_9` (`away_player_9`),
  KEY `away_player_10` (`away_player_10`),
  KEY `away_player_11` (`away_player_11`),
  CONSTRAINT `Event_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `Country` (`id`),
  CONSTRAINT `Event_ibfk_10` FOREIGN KEY (`home_player_6`) REFERENCES `Player` (`player_api_id`),
  CONSTRAINT `Event_ibfk_11` FOREIGN KEY (`home_player_7`) REFERENCES `Player` (`player_api_id`),
  CONSTRAINT `Event_ibfk_12` FOREIGN KEY (`home_player_8`) REFERENCES `Player` (`player_api_id`),
  CONSTRAINT `Event_ibfk_13` FOREIGN KEY (`home_player_9`) REFERENCES `Player` (`player_api_id`),
  CONSTRAINT `Event_ibfk_14` FOREIGN KEY (`home_player_10`) REFERENCES `Player` (`player_api_id`),
  CONSTRAINT `Event_ibfk_15` FOREIGN KEY (`home_player_11`) REFERENCES `Player` (`player_api_id`),
  CONSTRAINT `Event_ibfk_16` FOREIGN KEY (`away_player_1`) REFERENCES `Player` (`player_api_id`),
  CONSTRAINT `Event_ibfk_17` FOREIGN KEY (`away_player_2`) REFERENCES `Player` (`player_api_id`),
  CONSTRAINT `Event_ibfk_18` FOREIGN KEY (`away_player_3`) REFERENCES `Player` (`player_api_id`),
  CONSTRAINT `Event_ibfk_19` FOREIGN KEY (`away_player_4`) REFERENCES `Player` (`player_api_id`),
  CONSTRAINT `Event_ibfk_2` FOREIGN KEY (`league_id`) REFERENCES `League` (`id`),
  CONSTRAINT `Event_ibfk_20` FOREIGN KEY (`away_player_5`) REFERENCES `Player` (`player_api_id`),
  CONSTRAINT `Event_ibfk_21` FOREIGN KEY (`away_player_6`) REFERENCES `Player` (`player_api_id`),
  CONSTRAINT `Event_ibfk_22` FOREIGN KEY (`away_player_7`) REFERENCES `Player` (`player_api_id`),
  CONSTRAINT `Event_ibfk_23` FOREIGN KEY (`away_player_8`) REFERENCES `Player` (`player_api_id`),
  CONSTRAINT `Event_ibfk_24` FOREIGN KEY (`away_player_9`) REFERENCES `Player` (`player_api_id`),
  CONSTRAINT `Event_ibfk_25` FOREIGN KEY (`away_player_10`) REFERENCES `Player` (`player_api_id`),
  CONSTRAINT `Event_ibfk_26` FOREIGN KEY (`away_player_11`) REFERENCES `Player` (`player_api_id`),
  CONSTRAINT `Event_ibfk_3` FOREIGN KEY (`home_team_api_id`) REFERENCES `Team` (`team_api_id`),
  CONSTRAINT `Event_ibfk_4` FOREIGN KEY (`away_team_api_id`) REFERENCES `Team` (`team_api_id`),
  CONSTRAINT `Event_ibfk_5` FOREIGN KEY (`home_player_1`) REFERENCES `Player` (`player_api_id`),
  CONSTRAINT `Event_ibfk_6` FOREIGN KEY (`home_player_2`) REFERENCES `Player` (`player_api_id`),
  CONSTRAINT `Event_ibfk_7` FOREIGN KEY (`home_player_3`) REFERENCES `Player` (`player_api_id`),
  CONSTRAINT `Event_ibfk_8` FOREIGN KEY (`home_player_4`) REFERENCES `Player` (`player_api_id`),
  CONSTRAINT `Event_ibfk_9` FOREIGN KEY (`home_player_5`) REFERENCES `Player` (`player_api_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25980 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `League`
--

DROP TABLE IF EXISTS `League`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `League` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `country_id` (`country_id`),
  CONSTRAINT `League_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `Country` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24559 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Player`
--

DROP TABLE IF EXISTS `Player`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Player` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `player_api_id` int(11) DEFAULT NULL,
  `player_name` varchar(100) DEFAULT NULL,
  `player_fifa_api_id` int(11) DEFAULT NULL,
  `birthday` varchar(100) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `player_api_id` (`player_api_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11076 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Player_Attributes`
--

DROP TABLE IF EXISTS `Player_Attributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Player_Attributes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `player_fifa_api_id` int(11) DEFAULT NULL,
  `player_api_id` int(11) DEFAULT NULL,
  `date` varchar(100) DEFAULT NULL,
  `overall_rating` int(11) DEFAULT NULL,
  `potential` int(11) DEFAULT NULL,
  `preferred_foot` varchar(100) DEFAULT NULL,
  `attacking_work_rate` varchar(100) DEFAULT NULL,
  `defensive_work_rate` varchar(100) DEFAULT NULL,
  `crossing` int(11) DEFAULT NULL,
  `finishing` int(11) DEFAULT NULL,
  `heading_accuracy` int(11) DEFAULT NULL,
  `short_passing` int(11) DEFAULT NULL,
  `volleys` int(11) DEFAULT NULL,
  `dribbling` int(11) DEFAULT NULL,
  `curve` int(11) DEFAULT NULL,
  `free_kick_accuracy` int(11) DEFAULT NULL,
  `long_passing` int(11) DEFAULT NULL,
  `ball_control` int(11) DEFAULT NULL,
  `acceleration` int(11) DEFAULT NULL,
  `sprint_speed` int(11) DEFAULT NULL,
  `agility` int(11) DEFAULT NULL,
  `reactions` int(11) DEFAULT NULL,
  `balance` int(11) DEFAULT NULL,
  `shot_power` int(11) DEFAULT NULL,
  `jumping` int(11) DEFAULT NULL,
  `stamina` int(11) DEFAULT NULL,
  `strength` int(11) DEFAULT NULL,
  `long_shots` int(11) DEFAULT NULL,
  `aggression` int(11) DEFAULT NULL,
  `interceptions` int(11) DEFAULT NULL,
  `positioning` int(11) DEFAULT NULL,
  `vision` int(11) DEFAULT NULL,
  `penalties` int(11) DEFAULT NULL,
  `marking` int(11) DEFAULT NULL,
  `standing_tackle` int(11) DEFAULT NULL,
  `sliding_tackle` int(11) DEFAULT NULL,
  `gk_diving` int(11) DEFAULT NULL,
  `gk_handling` int(11) DEFAULT NULL,
  `gk_kicking` int(11) DEFAULT NULL,
  `gk_positioning` int(11) DEFAULT NULL,
  `gk_reflexes` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `player_api_id` (`player_api_id`),
  CONSTRAINT `Player_Attributes_ibfk_1` FOREIGN KEY (`player_api_id`) REFERENCES `Player` (`player_api_id`)
) ENGINE=InnoDB AUTO_INCREMENT=183979 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Team`
--

DROP TABLE IF EXISTS `Team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team_api_id` int(11) DEFAULT NULL,
  `team_fifa_api_id` int(11) DEFAULT NULL,
  `team_long_name` varchar(100) DEFAULT NULL,
  `team_short_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `team_api_id` (`team_api_id`)
) ENGINE=InnoDB AUTO_INCREMENT=51607 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Team_Attributes`
--

DROP TABLE IF EXISTS `Team_Attributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Team_Attributes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team_fifa_api_id` int(11) DEFAULT NULL,
  `team_api_id` int(11) DEFAULT NULL,
  `date` varchar(100) DEFAULT NULL,
  `buildUpPlaySpeed` int(11) DEFAULT NULL,
  `buildUpPlaySpeedClass` varchar(100) DEFAULT NULL,
  `buildUpPlayDribbling` int(11) DEFAULT NULL,
  `buildUpPlayDribblingClass` varchar(100) DEFAULT NULL,
  `buildUpPlayPassing` int(11) DEFAULT NULL,
  `buildUpPlayPassingClass` varchar(100) DEFAULT NULL,
  `buildUpPlayPositioningClass` varchar(100) DEFAULT NULL,
  `chanceCreationPassing` int(11) DEFAULT NULL,
  `chanceCreationPassingClass` varchar(100) DEFAULT NULL,
  `chanceCreationCrossing` int(11) DEFAULT NULL,
  `chanceCreationCrossingClass` varchar(100) DEFAULT NULL,
  `chanceCreationShooting` int(11) DEFAULT NULL,
  `chanceCreationShootingClass` varchar(100) DEFAULT NULL,
  `chanceCreationPositioningClass` varchar(100) DEFAULT NULL,
  `defencePressure` int(11) DEFAULT NULL,
  `defencePressureClass` varchar(100) DEFAULT NULL,
  `defenceAggression` int(11) DEFAULT NULL,
  `defenceAggressionClass` varchar(100) DEFAULT NULL,
  `defenceTeamWidth` int(11) DEFAULT NULL,
  `defenceTeamWidthClass` varchar(100) DEFAULT NULL,
  `defenceDefenderLineClass` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `team_api_id` (`team_api_id`),
  CONSTRAINT `Team_Attributes_ibfk_1` FOREIGN KEY (`team_api_id`) REFERENCES `Team` (`team_api_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1459 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-13 10:05:06
