(function(){
    'use strict';

    angular.module('pssApp')
        .config(function ($translateProvider) {
            $translateProvider.translations('fr', {

                'application.title':"PSS",
                'application.menu.api':"API",
                'application.menu.sport':"Sport",
                'application.creationCriteria':"Critères de création",
                'application.managementCriteria':"Actions possibles",
                'application.creationButton':"Enregistrer",
                'application.menu.database':"Base de données",
                'season.getSeason':"Récupérer les saisons",
                'sport.create.success':"Le sport a été correctement créé.",
                'sport.create.error':"Une erreur est survenue lors de la création du sport.",
                'sport.title.creation':"Création d'un sport",
                'sport.creationCriteria.name':"Nom",
                'apiFootball.title.management':"Gestion de l'API Football",
                'apiFootball.updateCompetition':"Mise à jour des compétitions",
                'apiFootball.updateTeam':"Mise à jour des équipes",
                'apiFootball.updateStatistic':"Mise à jour des statistiques",
                'apiFootball.updateEvent':"Mise à jour des matchs",
                'apiFootball.searchCriteria.year':"Année",
                'application.menu.analysis':"Analyse",
                'statistic.updateAll.success':"Les statistiques ont bien été mis à jour.",
                'statistic.updateAll.error':"Une erreur est survenue lors de la mise à jour des statistiques.",
                'competition.updateAll.success':"Les compétitions ont bien été mises à jour.",
                'competition.updateAll.error':"Une erreur est survenue lors de la mise à jour des compétitions.",
                'team.updateAll.success':"Les équipes ont bien été mises à jour.",
                'team.updateAll.error':"Une erreur est survenue lors de la mise à jour des équipes.",
                'event.updateAll.success':"Les rencontres ont bien été mises à jour.",
                'event.updateAll.error':"Une erreur est survenue lors de la mise à jour des rencontres.",
                'event.updateOdd.success':"Les cotes ont bien été mises à jour.",
                'event.updateOdd.error':"Une erreur est survenue lors de la mise à jour des cotes.",
                'analysis.title.victory':"Analyse par victoire",
                'analysis.title.defeat':"Analyse par défaite",
                'analysis.title.draw':"Analyse par nul",
                'analysisByVictory.straightWins':"Victoire d'affilée",
                'analysisByVictory.analyze':"Analyser",
                'analysisByVictory.graph.victory':"Pourcentage gain match suivant série de victoire",
                'analysisByVictory.graph.defeat':"Pourcentage perte match suivant série de victoire",
                'analysisByVictory.graph.draw':"Pourcentage nul match suivant série de victoire",
                'analysisByVictory.graph.victoryProfitability':"Rentabilité gain match suivant série de victoire",
                'analysisByVictory.graph.defeatProfitability':"Rentabilité perte match suivant série de victoire",
                'analysisByVictory.graph.drawProfitability':"Rentabilité nul match suivant série de victoire",
                'analysisByDefeat.straightDefeats':"Défaite d'affilée",
                'analysisByDefeat.analyze':"Analyser",
                'analysisByDefeat.graph.victory':"Gain match suivant série de défaite",
                'analysisByDefeat.graph.defeat':"Perte match suivant série de défaite",
                'analysisByDefeat.graph.draw':"Nul match suivant série de défaite",
                'analysisByDefeat.graph.victoryProfitability':"Rentabilité gain match suivant série de défaite",
                'analysisByDefeat.graph.defeatProfitability':"Rentabilité perte match suivant série de défaite",
                'analysisByDefeat.graph.drawProfitability':"Rentabilité nul match suivant série de défaite",
                'analysisByDraw.straightDraws':"Nul d'affilé",
                'analysisByDraw.analyze':"Analyser",
                'analysisByDraw.graph.victory':"Gain match suivant série de nuls",
                'analysisByDraw.graph.defeat':"Perte match suivant série de nuls",
                'analysisByDraw.graph.draw':"Nul match suivant série de nuls",
                'databaseLoading.title.management':"Gestion des bases de données",
                'databaseLoading.updateCompetition':"Mise à jour des compétitions",
                'databaseLoading.updateEvent':"Mise à jour des rencontres",
                'databaseLoading.updateTeam':"Mise à jour des équipes",
                'databaseLoading.updateStatistic':"Mise à jour des statistiques",
                'databaseLoading.updateOdd':"Mise à jour des cotes",
                'dbTmp.match.getAll.error':"Une erreur est survenue lors du chargement des matchs de la base de données temporaire.",
                'dbTmp.team.getAll.error':"Une erreur est survenue lors du chargement des équipes de la base de données temporaire.",
                'dbTmp.match.fillCompetition.error':"Une erreur est survenue lors de l'alimentation des compétitions depuis la base de données temporaire.",
                'dbTmp.match.fillCompetition.success':"Les compétitions depuis la base de données temporaire ont bien été mises à jour.",
                'dbTmp.match.fillEvent.error':"Une erreur est survenue lors de l'alimentation des rencontres depuis la base de données temporaire.",
                'dbTmp.match.fillEvent.success':"Les rencontres depuis la base de données temporaire ont bien été mises à jour.",
                'dbTmp.team.fillTeam.error':"Une erreur est survenue lors de l'alimentation des équipes depuis la base de données temporaire ont bien été mises à jour.",
                'dbTmp.team.fillTeam.success':"Les équipes depuis la base de données temporaire ont bien été mises à jour."




            });

            $translateProvider.use('fr');

            $translateProvider.useSanitizeValueStrategy('sanitizeParameters');

        })
})(angular);
