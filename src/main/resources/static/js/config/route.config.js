(function (){

    angular.module('pssApp')
        .config(function ($httpProvider, $stateProvider) {
            $stateProvider
                .state('apiFootball', {
                    url: '/apiFootball',
                    templateUrl: '/view/apiFootball.view.html',
                    controller: 'apiFootballCtrl',
                    controllerAs: 'apiFootball'
                })
                .state('sport', {
                    url: '/sport',
                    templateUrl:'/view/sport.view.html',
                    controller:'sportCtrl',
                    controllerAs:'sport'
                })
                .state('analysisByVictory', {
                    url: '/analysisByVictory',
                    templateUrl: '/view/analysisByVictory.view.html',
                    controller: 'analysisByVictoryCtrl',
                    controllerAs: 'analysisByVictory'
                })
                .state('analysisByDefeat', {
                    url: '/analysisByDefeat',
                    templateUrl: '/view/analysisByDefeat.view.html',
                    controller: 'analysisByDefeatCtrl',
                    controllerAs: 'analysisByDefeat'
                })
                .state('analysisByDraw', {
                    url: '/analysisByDraw',
                    templateUrl: '/view/analysisByDraw.view.html',
                    controller: 'analysisByDrawCtrl',
                    controllerAs: 'analysisByDraw'
                })
                .state('databaseLoading', {
                    url: '/databaseLoading',
                    templateUrl: '/view/databaseLoading.view.html',
                    controller: 'databaseLoadingCtrl',
                    controllerAs: 'databaseLoading'
                })
        })



})(angular);