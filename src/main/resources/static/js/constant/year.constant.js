(function (){

    angular.module('pssApp')
        .constant('years', ['2016', '2017'])
        .constant('defaultYear', '2017')
})(angular);