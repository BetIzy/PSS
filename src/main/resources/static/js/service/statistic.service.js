(function(angular) {
    'use strict';

    angular.module('pssApp')
        .service('statisticService', function($q, ngToast, $http, $filter){

            return {
                updateAll: updateAll
            };

            /*function updateAll(statistics) {
                var deferred = $q.defer();

                $http.post('/statistic/updateAll', statistics).success(function (data) {
                    ngToast.success($filter('translate')('statistic.updateAll.success'));
                    deferred.resolve(data);
                }).error(function () {
                    ngToast.danger($filter('translate')('statistic.updateAll.error'));
                    deferred.reject();
                });

                return deferred.promise;
            }*/


            function updateAll() {
                var deferred = $q.defer();

                $http.post('/statistic/updateAll').success(function (data) {
                    ngToast.success($filter('translate')('statistic.updateAll.success'));
                    deferred.resolve(data);
                }).error(function () {
                    ngToast.danger($filter('translate')('statistic.updateAll.error'));
                    deferred.reject();
                });

                return deferred.promise;
            }
        })

})(angular);


