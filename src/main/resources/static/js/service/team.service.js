(function(angular) {
    'use strict';

    angular.module('pssApp')
        .service('teamService', function($q, ngToast, $http, $filter){

            return {
                updateAll: updateAll
            };

            function updateAll(teams) {
                var deferred = $q.defer();

                $http.post('/team/updateAll', teams).success(function (data) {
                    ngToast.success($filter('translate')('team.updateAll.success'));
                    deferred.resolve(data);
                }).error(function () {
                    ngToast.danger($filter('translate')('team.updateAll.error'));
                    deferred.reject();
                });

                return deferred.promise;
            }
        })

})(angular);


