(function(angular) {
    'use strict';

    angular.module('pssApp')
        .service('analysisByVictoryService', function($q, ngToast, $http, $filter){

            return {
                analyzeVictory : analyzeVictory,
                analyzeDefeat : analyzeDefeat,
                analyzeDraw : analyzeDraw
            };

            function analyzeDraw() {
                var deferred = $q.defer();
                $http.get('/analysisByVictory/analyseDraw').success(function (data) {
                    deferred.resolve(data);
                }).error(function () {
                    deferred.reject();
                });
                return deferred.promise;
            }

            function analyzeDefeat() {
                var deferred = $q.defer();
                $http.get('/analysisByVictory/analyseDefeat').success(function (data) {
                    deferred.resolve(data);
                }).error(function () {
                    deferred.reject();
                });
                return deferred.promise;
            }

            function analyzeVictory() {
                var deferred = $q.defer();
                $http.get('/analysisByVictory/analyseVictory').success(function (data) {
                    deferred.resolve(data);
                }).error(function () {
                    deferred.reject();
                });
                return deferred.promise;
            }

        })

})(angular);


