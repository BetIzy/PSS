(function(angular) {
    'use strict';

    angular.module('pssApp')
        .service('analysisByDrawService', function($q, ngToast, $http, $filter){

            return {
                analyzeVictory : analyzeVictory,
                analyzeDefeat : analyzeDefeat,
                analyzeDraw : analyzeDraw
            };

            function analyzeDraw() {
                var deferred = $q.defer();
                $http.get('/analysisByDraw/analyseDraw').success(function (data) {
                    deferred.resolve(data);
                }).error(function () {
                    deferred.reject();
                });
                return deferred.promise;
            }

            function analyzeDefeat() {
                var deferred = $q.defer();
                $http.get('/analysisByDraw/analyseDefeat').success(function (data) {
                    deferred.resolve(data);
                }).error(function () {
                    deferred.reject();
                });
                return deferred.promise;
            }

            function analyzeVictory() {
                var deferred = $q.defer();
                $http.get('/analysisByDraw/analyseVictory').success(function (data) {
                    deferred.resolve(data);
                }).error(function () {
                    deferred.reject();
                });
                return deferred.promise;
            }

        })

})(angular);


