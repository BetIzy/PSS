(function(angular) {
    'use strict';

    angular.module('pssApp')
        .service('sportService', function($q, ngToast, $http, $filter){

            return {
                create: create
            };

            function create(sport) {
                var deferred = $q.defer();

                $http.post('/sport/create', sport).success(function (data) {
                    ngToast.success($filter('translate')('sport.create.success'));
                    deferred.resolve(data);
                }).error(function () {
                    ngToast.danger($filter('translate')('sport.create.error'));
                    deferred.reject();
                });

                return deferred.promise;
            }
        })

})(angular);


