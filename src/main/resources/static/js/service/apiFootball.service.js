(function(angular) {
    'use strict';

    angular.module('pssApp')
        .service('apiFootballService', function($q, ngToast, $http, $filter,footballdataFactory){

            return {
                getCompetitions: getCompetitions,
                getTeams: getTeams,
                getStatistics: getStatistics,
                getEvents: getEvents
            };

            function getEvents(idSeason) {
                var deferred = $q.defer();

                footballdataFactory.getFixturesBySeason({
                    id: idSeason,
                    apiKey: '3a0d519ac88f46fd8b8c94e46abc53cd'
                }).success(function (data) {
                    deferred.resolve(data);
                }).error(function (error, status) {
                    if(status == 403 || error.error == "Ever seen a league table for that competition?"){
                        deferred.resolve();
                    }else{
                        deferred.reject();
                    }
                });

                return deferred.promise;
            }

            function getStatistics(idSeason) {
                var deferred = $q.defer();

                footballdataFactory.getLeagueTableBySeason({
                    id: idSeason,
                    apiKey: '3a0d519ac88f46fd8b8c94e46abc53cd'
                }).success(function (data) {
                    deferred.resolve(data);
                }).error(function (error, status) {
                    if(status == 403 || error.error == "Ever seen a league table for that competition?"){
                        deferred.resolve();
                    }else{
                        deferred.reject();
                    }
                });

                return deferred.promise;
            }

            function getTeams(idSeason){
                var deferred = $q.defer();

                footballdataFactory.getTeamsBySeason({
                    id: idSeason,
                    apiKey: '3a0d519ac88f46fd8b8c94e46abc53cd'
                }).success(function (data) {
                    deferred.resolve(data);
                }).error(function (error, status) {
                    if(status == 403 || error.error == "Ever seen a league table for that competition?"){
                        deferred.resolve();
                    }else{
                        deferred.reject();
                    }
                });

                return deferred.promise;
            }

            function getCompetitions(year) {
                var deferred = $q.defer();

                footballdataFactory.getSeasons({
                    season: year,
                    apiKey: '3a0d519ac88f46fd8b8c94e46abc53cd'
                }).success(function (data) {
                    deferred.resolve(data);
                }).error(function (error, status) {
                    if(status == 403 || error.error == "Ever seen a league table for that competition?"){
                        deferred.resolve();
                    }else{
                        deferred.reject();
                    }
                });

                return deferred.promise;
            }
        })

})(angular);


