(function(angular) {
    'use strict';

    angular.module('pssApp')
        .service('eventService', function($q, ngToast, $http, $filter){

            return {
                updateAll: updateAll,
                updateOdd: updateOdd
            };

            function updateOdd() {
                var deferred = $q.defer();

                $http.post('/event/updateOdd').success(function (data) {
                    ngToast.success($filter('translate')('event.updateOdd.success'));
                    deferred.resolve(data);
                }).error(function () {
                    ngToast.danger($filter('translate')('event.updateOdd.error'));
                    deferred.reject();
                });

                return deferred.promise;
            }

            function updateAll(events) {
                var deferred = $q.defer();

                $http.post('/event/updateAll', events).success(function (data) {
                    ngToast.success($filter('translate')('event.updateAll.success'));
                    deferred.resolve(data);
                }).error(function () {
                    ngToast.danger($filter('translate')('event.updateAll.error'));
                    deferred.reject();
                });

                return deferred.promise;
            }
        })

})(angular);


