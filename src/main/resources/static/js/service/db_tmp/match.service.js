(function(angular) {
    'use strict';

    angular.module('pssApp')
        .service('dbTmpMatchService', function($q, ngToast, $http, $filter){

            return {
                getAll: getAll,
                fillCompetition : fillCompetition,
                fillEvent : fillEvent
            };

            function fillEvent(matchs){
                var deferred = $q.defer();

                $http.post('/db_tmp/match/fillEvent', matchs).success(function (data) {
                    ngToast.success($filter('translate')('dbTmp.match.fillEvent.success'));
                    deferred.resolve(data);
                }).error(function () {
                    ngToast.danger($filter('translate')('dbTmp.match.fillEvent.error'));
                    deferred.reject();
                });

                return deferred.promise;
            }

            function getAll() {
                var deferred = $q.defer();

                $http.get('/db_tmp/match/getAll').success(function (data) {
                    deferred.resolve(data);
                }).error(function () {
                    ngToast.danger($filter('translate')('dbTmp.match.getAll.error'));
                    deferred.reject();
                });

                return deferred.promise;
            }

            function fillCompetition(matchs){
                var deferred = $q.defer();

                $http.post('/db_tmp/match/fillCompetition', matchs).success(function (data) {
                    ngToast.success($filter('translate')('dbTmp.match.fillCompetition.success'));
                    deferred.resolve(data);
                }).error(function () {
                    ngToast.danger($filter('translate')('dbTmp.match.fillCompetition.error'));
                    deferred.reject();
                });

                return deferred.promise;
            }

        })

})(angular);


