(function(angular) {
    'use strict';

    angular.module('pssApp')
        .service('dbTmpTeamService', function($q, ngToast, $http, $filter){

            return {
                fillTeam: fillTeam,
                getAll: getAll
            };

            function getAll(){
                var deferred = $q.defer();

                $http.get('db_tmp/team/getAll').success(function (data) {
                    deferred.resolve(data);
                }).error(function () {
                    ngToast.danger($filter('translate')('dbTmp.team.getAll.error'));
                    deferred.reject();
                });

                return deferred.promise;
            }

            function fillTeam(teams) {
                var deferred = $q.defer();

                $http.post('db_tmp/team/fillTeam', teams).success(function (data) {
                    ngToast.success($filter('translate')('dbTmp.team.fillTeam.success'));
                    deferred.resolve(data);
                }).error(function () {
                    ngToast.danger($filter('translate')('dbTmp.team.fillTeam.error'));
                    deferred.reject();
                });

                return deferred.promise;
            }
        })

})(angular);


