(function(angular) {
    'use strict';

    angular.module('pssApp')
        .service('competitionService', function($q, ngToast, $http, $filter){

            return {
                updateAll: updateAll
            };

            function updateAll(competitions) {
                var deferred = $q.defer();

                $http.post('/competition/updateAll', competitions).success(function (data) {
                    ngToast.success($filter('translate')('competition.updateAll.success'));
                    deferred.resolve(data);
                }).error(function () {
                    ngToast.danger($filter('translate')('competition.updateAll.error'));
                    deferred.reject();
                });

                return deferred.promise;
            }
        })

})(angular);


