(function () {

    angular.module('pssApp', ['pascalprecht.translate', 'ngSanitize', 'ui.router', 'ngToast', 'ngAnimate', 'ngMaterial', 'jtt_footballdata'])

        .controller('indexCtrl', function ($rootScope, $scope, $http) {
            $scope.logout = function(){
                $http.post('logout', {}).success(function() {
                    $rootScope.authenticated = false;
                    window.location = "/";
                }).error(function(data) {
                    $rootScope.authenticated = false;
                });
            }


        })

})(angular);