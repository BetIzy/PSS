(function () {
    'use strict';

    angular.module('pssApp')
        .controller('databaseLoadingCtrl', function(dbTmpMatchService, dbTmpTeamService, statisticService, eventService) {
            var databaseLoading = this;

            databaseLoading.updateCompetition = function(){
                dbTmpMatchService.getAll().then(function(data){
                    console.log(data);
                    dbTmpMatchService.fillCompetition(data).then(function (data) {
                        console.log(data);
                    })
                })
            }

            databaseLoading.updateEvent = function(){
                dbTmpMatchService.getAll().then(function (data) {
                    dbTmpMatchService.fillEvent(data).then(function (data) {
                        console.log(data);
                    })
                })
            }

            databaseLoading.updateTeam = function(){
                dbTmpTeamService.getAll().then(function (data) {
                    dbTmpTeamService.fillTeam(data).then(function (data) {
                        console.log(data);
                    })
                })
            }

            databaseLoading.updateStatistic = function(){
                statisticService.updateAll().then(function (data) {
                    console.log(data);
                })
            }

            databaseLoading.updateOdd = function(){
                eventService.updateOdd().then(function (data) {
                    console.log(data);
                })
            }

        })

})(angular);