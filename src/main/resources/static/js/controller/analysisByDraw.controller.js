(function () {
    'use strict';

    angular.module('pssApp')
        .controller('analysisByDrawCtrl', function(analysisByDrawService, $filter) {
            var analysisByDraw = this;
            var chart = null;
            var chartData = [];

            analysisByDraw.showGraph = false;

            analysisByDraw.analyze = function () {
                analysisByDraw.generateGraph();

                analysisByDrawService.analyzeVictory().then(function (data) {

                    analysisByDraw.generateVictoryGraph(data.filter(function (item) {
                        return item.percentWins > 0;
                    }));
                });
                analysisByDrawService.analyzeDefeat().then(function (data) {
                    analysisByDraw.generateDefeatGraph(data.filter(function (item) {
                        return item.percentWins > 0;
                    }));
                });
                analysisByDrawService.analyzeDraw().then(function (data) {
                    analysisByDraw.generateDrawGraph(data.filter(function (item) {
                        return item.percentWins > 0;
                    }));
                })
            };

            analysisByDraw.generateGraph = function () {
                chart = AmCharts.makeChart("analysisByDraw", {
                    "type": "serial",
                    "categoryField": "straightDraws",
                    "dataProvider":chartData,
                    "legend":{}
                })
            };

            analysisByDraw.generateVictoryGraph = function (data) {
                var graph = new AmCharts.AmGraph();
                graph.title = $filter('translate')('analysisByDraw.graph.victory');
                graph.valueField = "percentWinsVictory";
                graph.balloonFunction = function (graphDataItem, graph) {
                    var value = graphDataItem.values.value;
                    var category = graphDataItem.category;
                    var nbCandidate = graphDataItem.dataContext.nbCandidate;
                    return "Pourcentage de victoire après "+category+" nuls : <b><span style='font-size:14px;'>"+value+"</span></b> ("+nbCandidate+")";
                };                graph.bulletSize = 8;
                graph.lineThickness = 2;
                graph.bullet = "round";
                graph.type = "smoothedLine";
                data.forEach(function (item, index) {
                    if(chartData.length > index){
                        chartData[index]["percentWinsVictory"] = item.percentWins;
                    }else{
                        chartData.push({
                            "straightDraws": item.straightDraws,
                            "percentWinsVictory":item.percentWins,
                            "nbCandidate":item.nbCandidate
                        })
                    }
                });
                chart.addGraph(graph);
                analysisByDraw.showGraph = true;
            };

            analysisByDraw.generateDefeatGraph = function (data) {
                var graph = new AmCharts.AmGraph();
                graph.title = $filter('translate')('analysisByDraw.graph.defeat');
                graph.valueField = "percentWinsDefeat";
                graph.balloonFunction = function (graphDataItem, graph) {
                    var value = graphDataItem.values.value;
                    var category = graphDataItem.category;
                    var nbCandidate = graphDataItem.dataContext.nbCandidate;
                    return "Pourcentage de défaite après "+category+" nuls : <b><span style='font-size:14px;'>"+value+"</span></b> ("+nbCandidate+")";
                };                graph.bulletSize = 8;
                graph.lineThickness = 2;
                graph.bullet = "round";
                graph.type = "smoothedLine";
                data.forEach(function (item, index) {
                    if(chartData.length > index){
                        chartData[index]["percentWinsDefeat"] = item.percentWins;
                    }else{
                        chartData.push({
                            "straightDraws": item.straightDraws,
                            "percentWinsDefeat":item.percentWins,
                            "nbCandidate":item.nbCandidate
                        })
                    }
                });
                chart.addGraph(graph);
                analysisByDraw.showGraph = true;

            };

            analysisByDraw.generateDrawGraph = function (data) {
                var graph = new AmCharts.AmGraph();
                graph.title = $filter('translate')('analysisByDraw.graph.draw');
                graph.valueField = "percentWinsDraw";
                graph.balloonFunction = function (graphDataItem, graph) {
                    var value = graphDataItem.values.value;
                    var category = graphDataItem.category;
                    var nbCandidate = graphDataItem.dataContext.nbCandidate;
                    return "Pourcentage de match nul après "+category+" nuls : <b><span style='font-size:14px;'>"+value+"</span></b> ("+nbCandidate+")";
                };                graph.bulletSize = 8;
                graph.lineThickness = 2;
                graph.bullet = "round";
                graph.type = "smoothedLine";
                data.forEach(function (item, index) {
                    if(chartData.length > index){
                        chartData[index]["percentWinsDraw"] = item.percentWins;
                    }else{
                        chartData.push({
                            "straightDraws": item.straightDraws,
                            "percentWinsDraw":item.percentWins,
                            "nbCandidate":item.nbCandidate
                        })
                    }
                });
                chart.addGraph(graph);
                analysisByDraw.showGraph = true;

            }

        })

})(angular);