(function () {
    'use strict';

    angular.module('pssApp')
        .controller('analysisByVictoryCtrl', function(analysisByVictoryService, $filter) {
            var analysisByVictory = this;
            var chart = null;
            var chartData = [];

            analysisByVictory.showGraph = false;

            analysisByVictory.analyze = function () {
                analysisByVictory.generateGraph();

                analysisByVictoryService.analyzeVictory().then(function (data) {

                    analysisByVictory.generateVictoryGraph(data.filter(function (item) {
                        return item.percentWins > 0;
                    }));
                    analysisByVictory.generateVictoryProfitabilityGraph();
                });
                analysisByVictoryService.analyzeDefeat().then(function (data) {
                    analysisByVictory.generateDefeatGraph(data.filter(function (item) {
                        return item.percentWins > 0;
                    }));
                    analysisByVictory.generateDefeatProfitabilityGraph();
                });
                analysisByVictoryService.analyzeDraw().then(function (data) {
                    analysisByVictory.generateDrawGraph(data.filter(function (item) {
                        return item.percentWins > 0;
                    }));
                    analysisByVictory.generateDrawProfitabilityGraph();
                })
            };
            
            analysisByVictory.generateGraph = function () {
                chart = AmCharts.makeChart("analysisByVictory", {
                    "type": "serial",
                    "categoryField": "straightWins",
                    "dataProvider":chartData,
                    "legend":{}
                })
            };

            analysisByVictory.generateVictoryProfitabilityGraph = function () {
                var graph = new AmCharts.AmGraph();
                graph.title = $filter('translate')('analysisByVictory.graph.victoryProfitability');
                graph.valueField = "profitabilityVictory";
                graph.balloonFunction = function (graphDataItem, graph) {
                    var straightWins = graphDataItem.dataContext.straightWins;
                    var profitabilityVictory = graphDataItem.dataContext.profitabilityVictory;
                    var nbCandidate = graphDataItem.dataContext.nbCandidate;
                    return "Rentabilité de jouer une victoire après "+straightWins+" victoires : <b><span style='font-size:14px;'>"+profitabilityVictory+"%</span></b> ("+nbCandidate+")";
                };
                graph.lineThickness = 2;
                graph.bullet = "round";
                graph.type = "smoothedLine";
                chart.addGraph(graph);
            };

            analysisByVictory.generateDefeatProfitabilityGraph = function () {
                var graph = new AmCharts.AmGraph();
                graph.title = $filter('translate')('analysisByVictory.graph.defeatProfitability');
                graph.valueField = "profitabilityDefeat";
                graph.balloonFunction = function (graphDataItem, graph) {
                    var straightWins = graphDataItem.dataContext.straightWins;
                    var profitabilityDefeat = graphDataItem.dataContext.profitabilityDefeat;
                    var nbCandidate = graphDataItem.dataContext.nbCandidateVictory;
                    return "Rentabilité de jouer une défaite après "+straightWins+" victoires : <b><span style='font-size:14px;'>"+profitabilityDefeat+"%</span></b> ("+nbCandidate+")";
                };
                graph.lineThickness = 2;
                graph.bullet = "round";
                graph.type = "smoothedLine";
                chart.addGraph(graph);
            };

            analysisByVictory.generateDrawProfitabilityGraph = function () {
                var graph = new AmCharts.AmGraph();
                graph.title = $filter('translate')('analysisByVictory.graph.drawProfitability');
                graph.valueField = "profitabilityDraw";
                graph.balloonFunction = function (graphDataItem, graph) {
                    var straightWins = graphDataItem.dataContext.straightWins;
                    var profitabilityDraw = graphDataItem.dataContext.profitabilityDraw;
                    var nbCandidate = graphDataItem.dataContext.nbCandidate;
                    return "Rentabilité de jouer un nul après "+straightWins+" victoires : <b><span style='font-size:14px;'>"+profitabilityDraw+"%</span></b> ("+nbCandidate+")";
                };
                graph.lineThickness = 2;
                graph.bullet = "round";
                graph.type = "smoothedLine";
                chart.addGraph(graph);
            };

            analysisByVictory.generateVictoryGraph = function (data) {
                var graph = new AmCharts.AmGraph();
                graph.title = $filter('translate')('analysisByVictory.graph.victory');
                graph.valueField = "percentWinsVictory";
                graph.hidden = true;
                graph.balloonFunction = function (graphDataItem, graph) {
                    var percentWinsVictory = graphDataItem.dataContext.percentWinsVictory;
                    var straightWins = graphDataItem.dataContext.straightWins;
                    var nbCandidate = graphDataItem.dataContext.nbCandidate;
                    return "Pourcentage de victoire après "+straightWins+" victoires : <b><span style='font-size:14px;'>"+percentWinsVictory+"%</span></b> ("+nbCandidate+")";
                };                graph.bulletSize = 8;
                graph.lineThickness = 2;
                graph.bullet = "round";
                graph.type = "smoothedLine";
                data.forEach(function (item, index) {
                   if(chartData.length > index){
                       chartData[index]["percentWinsVictory"] = item.percentWins;
                       chartData[index]["profitabilityVictory"] = item.profitability;
                   }else{
                       chartData.push({
                           "straightWins": item.straightWins,
                           "percentWinsVictory":item.percentWins,
                           "nbCandidate":item.nbCandidate,
                           "profitabilityVictory":item.profitability
                       })
                   }
                });
                chart.addGraph(graph);
                analysisByVictory.showGraph = true;
            };

            analysisByVictory.generateDefeatGraph = function (data) {
                var graph = new AmCharts.AmGraph();
                graph.title = $filter('translate')('analysisByVictory.graph.defeat');
                graph.valueField = "percentWinsDefeat";
                graph.hidden = true;
                graph.balloonFunction = function (graphDataItem, graph) {
                    var percentWinsDefeat = graphDataItem.dataContext.percentWinsDefeat;
                    var straightWins = graphDataItem.dataContext.straightWins;
                    var nbCandidate = graphDataItem.dataContext.nbCandidate;
                    return "Pourcentage de défaite après "+straightWins+" victoires : <b><span style='font-size:14px;'>"+percentWinsDefeat+"%</span></b> ("+nbCandidate+")";
                };                graph.bulletSize = 8;
                graph.lineThickness = 2;
                graph.bullet = "round";
                graph.type = "smoothedLine";
                data.forEach(function (item, index) {
                    if(chartData.length > index){
                        chartData[index]["percentWinsDefeat"] = item.percentWins;
                        chartData[index]["profitabilityDefeat"] = item.profitability;
                    }else{
                        chartData.push({
                            "straightWins": item.straightWins,
                            "percentWinsDefeat":item.percentWins,
                            "nbCandidate":item.nbCandidate,
                            "profitabilityDefeat":item.profitability
                        })
                    }
                });
                chart.addGraph(graph);
                analysisByVictory.showGraph = true;

            };

            analysisByVictory.generateDrawGraph = function (data) {
                var graph = new AmCharts.AmGraph();
                graph.title = $filter('translate')('analysisByVictory.graph.draw');
                graph.valueField = "percentWinsDraw";
                graph.hidden = true;
                graph.balloonFunction = function (graphDataItem, graph) {
                    var percentWinsDraw = graphDataItem.dataContext.percentWinsDraw;
                    var straightWins = graphDataItem.dataContext.straightWins;
                    var nbCandidate = graphDataItem.dataContext.nbCandidate;
                    return "Pourcentage de match nul après "+straightWins+" victoires : <b><span style='font-size:14px;'>"+percentWinsDraw+"%</span></b> ("+nbCandidate+")";
                };                graph.bulletSize = 8;
                graph.lineThickness = 2;
                graph.bullet = "round";
                graph.type = "smoothedLine";
                data.forEach(function (item, index) {
                    if(chartData.length > index){
                        chartData[index]["percentWinsDraw"] = item.percentWins;
                        chartData[index]["profitabilityDraw"] = item.profitability;
                    }else{
                        chartData.push({
                            "straightWins": item.straightWins,
                            "percentWinsDraw":item.percentWins,
                            "nbCandidate":item.nbCandidate,
                            "profitabilityDraw":item.profitability
                        })
                    }
                });
                chart.addGraph(graph);
                analysisByVictory.showGraph = true;
                console.log(chart);

            }

        })

})(angular);