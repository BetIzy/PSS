(function () {
    'use strict';

    angular.module('pssApp')
        .controller('apiFootballCtrl', function($q, apiFootballService, competitionService, teamService, statisticService, eventService, years, defaultYear) {
            var apiFootball = this;

            apiFootball.years = years;
            apiFootball.yearSelected = defaultYear;

            apiFootball.updateStatistic = function () {
                apiFootballService.getCompetitions(apiFootball.yearSelected).then(function(competitions){
                  var statistics = [];
                  var index = 0;
                  competitions.forEach(function (competition) {
                      apiFootballService.getStatistics(competition.id).then(function (data) {
                          index++;
                          if(data != null && data.standing != null){
                              data.standing.forEach(function (team) {
                                  if (statistics.filter(function (teamToIterate) {
                                          return teamToIterate.teamName == team.teamName && competition.id == teamToIterate.competitionId;
                                      }).length == 0) {
                                      statistics.push({
                                          goalsHome: team.home.goals,
                                          goalsAway: team.away.goals,
                                          winsHome: team.home.wins,
                                          winsAway: team.away.wins,
                                          lossesHome: team.home.losses,
                                          lossesAway: team.away.losses,
                                          drawsHome: team.home.draws,
                                          drawsAway: team.away.draws,
                                          teamName: team.teamName,
                                          competitionId: competition.id
                                      });
                                  }
                              })
                          }
                          if(index == competitions.length - 1){
                              statisticService.updateAll(statistics);
                          }
                      })
                  })
              });
            };

            apiFootball.updateEvent = function () {
                apiFootballService.getCompetitions(apiFootball.yearSelected).then(function(competitions){
                    var events = [];
                    var index = 0;
                    competitions.forEach(function (competition) {
                        apiFootballService.getEvents(competition.id).then(function (data) {
                            index++;
                            if(data != null && data.fixtures != null){
                                data.fixtures.forEach(function (event) {
                                    var eventToPush = {
                                        teamHomeName: event.homeTeamName,
                                        teamAwayName: event.awayTeamName,
                                        date: new Date(event.date),
                                        goalsHome: event.result.goalsHomeTeam,
                                        goalsAway: event.result.goalsAwayTeam,
                                        competition: {
                                            competitionId: competition.id,
                                            name: competition.caption,
                                            season: competition.year + '/' + (new Number(competition.year) + 1)
                                        }
                                    };
                                    if(event.result.halfTime != null){
                                        eventToPush.goalsAwayHt = event.result.halfTime.goalsAwayTeam;
                                        eventToPush.goalsHomeHt = event.result.halfTime.goalsHomeTeam;
                                    }
                                    events.push(eventToPush);
                                })
                            }
                            if(index == competitions.length - 1){
                                eventService.updateAll(events);
                            }
                        })
                    })
                })
            };

            apiFootball.updateCompetition = function () {
                apiFootballService.getCompetitions(apiFootball.yearSelected).then(function(data){
                    var competitions = [];
                    data.forEach(function (competition) {
                        competitions.push({
                            competitionId: competition.id,
                            name: competition.caption,
                            season: competition.year+'/'+(new Number(competition.year)+1)
                        })
                    });
                    competitionService.updateAll(competitions);
                });
            };

            apiFootball.updateTeam = function(){
                apiFootballService.getCompetitions(apiFootball.yearSelected).then(function(competitions){
                    var teams = [];
                    var index = 0;
                    competitions.forEach(function (competition) {
                        apiFootballService.getTeams(competition.id).then(function(data){
                            index++;
                            if(data != null) {
                                data.teams.forEach(function (team) {
                                    if (teams.filter(function (teamToIterate) {
                                            return teamToIterate.name == team.name;
                                        }).length == 0) {
                                        teams.push({
                                            name: team.name,
                                            shortName: team.shortName
                                        })
                                    }
                                });
                            }
                            if(index == competitions.length - 1){
                                teamService.updateAll(teams);
                            }
                        })
                    });
                });
            };


        })

})(angular);