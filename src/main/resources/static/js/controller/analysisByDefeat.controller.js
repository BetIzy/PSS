(function () {
    'use strict';

    angular.module('pssApp')
        .controller('analysisByDefeatCtrl', function(analysisByDefeatService, $filter) {
            var analysisByDefeat = this;
            var chart = null;
            var chartData = [];

            analysisByDefeat.showGraph = false;

            analysisByDefeat.analyze = function () {
                analysisByDefeat.generateGraph();

                analysisByDefeatService.analyzeVictory().then(function (data) {

                    analysisByDefeat.generateVictoryGraph(data.filter(function (item) {
                        return item.percentWins > 0;
                    }));
                    analysisByDefeat.generateVictoryProfitabilityGraph();
                });
                analysisByDefeatService.analyzeDefeat().then(function (data) {
                    analysisByDefeat.generateDefeatGraph(data.filter(function (item) {
                        return item.percentWins > 0;
                    }));
                    analysisByDefeat.generateDefeatProfitabilityGraph();

                });
                analysisByDefeatService.analyzeDraw().then(function (data) {
                    analysisByDefeat.generateDrawGraph(data.filter(function (item) {
                        return item.percentWins > 0;
                    }));
                    analysisByDefeat.generateDrawProfitabilityGraph();

                })
            };

            analysisByDefeat.generateGraph = function () {
                chart = AmCharts.makeChart("analysisByDefeat", {
                    "type": "serial",
                    "categoryField": "straightDefeats",
                    "dataProvider":chartData,
                    "legend":{}
                })
            };

            analysisByDefeat.generateVictoryProfitabilityGraph = function () {
                var graph = new AmCharts.AmGraph();
                graph.title = $filter('translate')('analysisByDefeat.graph.victoryProfitability');
                graph.valueField = "profitabilityVictory";
                graph.balloonFunction = function (graphDataItem, graph) {
                    var straightDefeats = graphDataItem.dataContext.straightDefeats;
                    var profitabilityVictory = graphDataItem.dataContext.profitabilityVictory;
                    var nbCandidate = graphDataItem.dataContext.nbCandidate;
                    return "Rentabilité de jouer une victoire après "+straightDefeats+" défaites : <b><span style='font-size:14px;'>"+profitabilityVictory+"%</span></b> ("+nbCandidate+")";
                };
                graph.lineThickness = 2;
                graph.bullet = "round";
                graph.type = "smoothedLine";
                chart.addGraph(graph);
            };

            analysisByDefeat.generateDefeatProfitabilityGraph = function () {
                var graph = new AmCharts.AmGraph();
                graph.title = $filter('translate')('analysisByDefeat.graph.defeatProfitability');
                graph.valueField = "profitabilityDefeat";
                graph.balloonFunction = function (graphDataItem, graph) {
                    var straightDefeats = graphDataItem.dataContext.straightDefeats;
                    var profitabilityDefeat = graphDataItem.dataContext.profitabilityDefeat;
                    var nbCandidate = graphDataItem.dataContext.nbCandidate;
                    return "Rentabilité de jouer une défaite après "+straightDefeats+" défaites : <b><span style='font-size:14px;'>"+profitabilityDefeat+"%</span></b> ("+nbCandidate+")";
                };
                graph.lineThickness = 2;
                graph.bullet = "round";
                graph.type = "smoothedLine";
                chart.addGraph(graph);
            };

            analysisByDefeat.generateDrawProfitabilityGraph = function () {
                var graph = new AmCharts.AmGraph();
                graph.title = $filter('translate')('analysisByDefeat.graph.drawProfitability');
                graph.valueField = "profitabilityDraw";
                graph.balloonFunction = function (graphDataItem, graph) {
                    var straightDefeats = graphDataItem.dataContext.straightDefeats;
                    var profitabilityDraw = graphDataItem.dataContext.profitabilityDraw;
                    var nbCandidate = graphDataItem.dataContext.nbCandidate;
                    return "Rentabilité de jouer un nul après "+straightDefeats+" défaites : <b><span style='font-size:14px;'>"+profitabilityDraw+"%</span></b> ("+nbCandidate+")";
                };
                graph.lineThickness = 2;
                graph.bullet = "round";
                graph.type = "smoothedLine";
                chart.addGraph(graph);
            };

            analysisByDefeat.generateVictoryGraph = function (data) {
                var graph = new AmCharts.AmGraph();
                graph.title = $filter('translate')('analysisByDefeat.graph.victory');
                graph.valueField = "percentWinsVictory";
                graph.hidden = true;
                graph.balloonFunction = function (graphDataItem, graph) {
                    var percentWinsVictory = graphDataItem.dataContext.percentWinsVictory;
                    var straightDefeats = graphDataItem.dataContext.straightDefeats;
                    var nbCandidate = graphDataItem.dataContext.nbCandidate;
                    return "Pourcentage de victoire après "+straightDefeats+" défaites : <b><span style='font-size:14px;'>"+percentWinsVictory+"</span></b> ("+nbCandidate+")";
                };
                graph.bulletSize = 8;
                graph.lineThickness = 2;
                graph.bullet = "round";
                graph.type = "smoothedLine";
                data.forEach(function (item, index) {
                    if(chartData.length > index){
                        chartData[index]["percentWinsVictory"] = item.percentWins;
                        chartData[index]["profitabilityVictory"] = item.profitability;
                    }else{
                        chartData.push({
                            "straightDefeats": item.straightDefeats,
                            "percentWinsVictory":item.percentWins,
                            "nbCandidate":item.nbCandidate,
                            "profitabilityVictory":item.profitability
                        })
                    }
                });
                chart.addGraph(graph);
                analysisByDefeat.showGraph = true;
            };

            analysisByDefeat.generateDefeatGraph = function (data) {
                var graph = new AmCharts.AmGraph();
                graph.title = $filter('translate')('analysisByDefeat.graph.defeat');
                graph.valueField = "percentWinsDefeat";
                graph.hidden = true;
                graph.balloonFunction = function (graphDataItem, graph) {
                    var percentWinsDefeat = graphDataItem.dataContext.percentWinsDefeat;
                    var straightDefeats = graphDataItem.dataContext.straightDefeats;
                    var nbCandidate = graphDataItem.dataContext.nbCandidate;
                    return "Pourcentage de défaite après "+straightDefeats+" défaites : <b><span style='font-size:14px;'>"+percentWinsDefeat+"</span></b> ("+nbCandidate+")";
                };
                graph.bulletSize = 8;
                graph.lineThickness = 2;
                graph.bullet = "round";
                graph.type = "smoothedLine";
                data.forEach(function (item, index) {
                    if(chartData.length > index){
                        chartData[index]["percentWinsDefeat"] = item.percentWins;
                        chartData[index]["profitabilityDefeat"] = item.profitability;
                    }else{
                        chartData.push({
                            "straightDefeats": item.straightDefeats,
                            "percentDefeatsDefeat":item.percentDefeats,
                            "nbCandidate":item.nbCandidate,
                            "profitabilityDefeat":item.profitability
                        })
                    }
                });
                chart.addGraph(graph);
                analysisByDefeat.showGraph = true;

            };

            analysisByDefeat.generateDrawGraph = function (data) {
                var graph = new AmCharts.AmGraph();
                graph.title = $filter('translate')('analysisByDefeat.graph.draw');
                graph.valueField = "percentWinsDraw";
                graph.hidden = true;
                graph.balloonFunction = function (graphDataItem, graph) {
                    var percentWinsDraw = graphDataItem.dataContext.percentWinsDraw;
                    var straightDefeats = graphDataItem.dataContext.straightDefeats;
                    var nbCandidate = graphDataItem.dataContext.nbCandidate;
                    return "Pourcentage de match nul après "+straightDefeats+" défaites : <b><span style='font-size:14px;'>"+percentWinsDraw+"</span></b> ("+nbCandidate+")";
                };
                graph.bulletSize = 8;
                graph.lineThickness = 2;
                graph.bullet = "round";
                graph.type = "smoothedLine";
                data.forEach(function (item, index) {
                    if(chartData.length > index){
                        chartData[index]["percentWinsDraw"] = item.percentWins;
                        chartData[index]["profitabilityDraw"] = item.profitability;
                    }else{
                        chartData.push({
                            "straightDefeats": item.straightDefeats,
                            "percentWinsDraw":item.percentWins,
                            "nbCandidate":item.nbCandidate,
                            "profitabilityDraw":item.profitability
                        })
                    }
                });
                chart.addGraph(graph);
                analysisByDefeat.showGraph = true;
                console.log(chart);
            }

        })

})(angular);