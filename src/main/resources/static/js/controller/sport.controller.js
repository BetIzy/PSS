(function () {
    'use strict';

    angular.module('pssApp')
        .controller('sportCtrl', function(sportService) {
            var sport = this;

            sport.create = function () {
                if(sport.createForm.$valid){
                    var sportToCreate = {
                        name: sport.name
                    };
                    sportService.create(sport);
                }
            }

        })

})(angular);