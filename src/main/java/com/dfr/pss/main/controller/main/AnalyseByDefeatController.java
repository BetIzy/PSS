package com.dfr.pss.main.controller.main;

import com.dfr.pss.main.bean.main.AnalyseByDefeatBean;
import com.dfr.pss.main.entity.main.Event;
import com.dfr.pss.main.entity.main.Team;
import com.dfr.pss.main.repository.main.EventRepository;
import com.dfr.pss.main.repository.main.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RequestMapping("/analysisByDefeat")
@Controller
public class AnalyseByDefeatController {

    @Autowired
    TeamRepository teamRepository;

    @Autowired
    EventRepository eventRepository;

    @GetMapping (value = "/analyseDefeat")
    @ResponseBody
    public List<AnalyseByDefeatBean> analyzeDefeat(){
        List<Team> teamList = teamRepository.findAll();
        Double defeat = new Double(0);
        Integer potential = 0;
        Double moneyEarned = new Double(0);
        List<AnalyseByDefeatBean> analyseByDefeatBeans = new ArrayList<AnalyseByDefeatBean>();
        List<AnalyseByDefeatBean> results = new ArrayList<AnalyseByDefeatBean>();
        int maxEventListSize = 0;
        for(Team team : teamList){
            //eventList.addAll(eventRepository.findByTeamAwayEntityOrTeamHomeEntityOrderByDate(team, team));
            //eventRepository.save(new Event());
            List<Event> eventList = eventRepository.findByTeamAwayEntityOrTeamHomeEntityOrderByDate(team, team);
            if(eventList.size() > maxEventListSize){
                maxEventListSize = eventList.size();
            }
            for(int straightWins = 1; straightWins < eventList.size()-1; straightWins++) {
                defeat = new Double(0);
                potential = 0;
                moneyEarned = new Double(0);
                for (int i = 0; i < eventList.size() - straightWins; i++) {
                    if (seriesDefeat(straightWins, 0, eventList, i, team)) {
                        defeat++;

                        if(eventList.get(straightWins).getTeamHomeEntity() == team){
                            if(eventList.get(straightWins).getOddAway() != null){
                                moneyEarned += eventList.get(straightWins).getOddAway();
                            }else{
                                moneyEarned++;
                            }
                        }else{
                            if(eventList.get(straightWins).getOddHome() != null){
                                moneyEarned += eventList.get(straightWins).getOddHome();
                            }else{
                                moneyEarned++;
                            }
                        }
                    }
                }
                for (int i = 0; i < eventList.size() - straightWins; i++) {
                    if (seriesDefeat(straightWins - 1, 0, eventList, i, team)) {
                        potential++;
                    }
                }
                if(potential != 0){
                    analyseByDefeatBeans.add(new AnalyseByDefeatBean(100*defeat/potential, straightWins, potential,potential-moneyEarned));
                }
            }
        }

        for(int i = 0; i < maxEventListSize; i++){
            final int iFinal = i;
            List<AnalyseByDefeatBean> analyseByDefeatBeansByStraightWins = analyseByDefeatBeans.stream().filter(bean -> bean.getStraightDefeats() == iFinal).collect(Collectors.toList());
            Double average = new Double(0);
            Integer nbCandidate = 0;
            Double profitability = new Double(0);
            for(AnalyseByDefeatBean analyseByDefeatBean : analyseByDefeatBeansByStraightWins){
                average += analyseByDefeatBean.getPercentWins()*analyseByDefeatBean.getNbCandidate();
                nbCandidate += analyseByDefeatBean.getNbCandidate();
                profitability += analyseByDefeatBean.getProfitability();
            }
            average = average/nbCandidate;
            results.add(new AnalyseByDefeatBean(average, i, nbCandidate,100*(profitability)/nbCandidate));
        }
        return results;
    }

    public boolean seriesDefeat(int straightWins, int currentSeries, List<Event> eventList, int index, Team team){
        if(currentSeries == straightWins){
            if(eventList.get(index).getTeamHomeName().equals(team.getName())){
                if(eventList.get(index).getGoalsHome() < eventList.get(index).getGoalsAway()){
                    //condition vérifié
                    return true;
                }else{
                    //condition pas vérifié
                    return false;
                }
                // cas de vérification possible
            }else {
                if(eventList.get(index).getGoalsHome() > eventList.get(index).getGoalsAway()){
                    //condition vérifié
                    return true;
                }else{
                    //condition pas vérifié
                    return false;
                }
                // cas de vérification possible
            }
        }else {
            if(eventList.get(index).getTeamHomeName().equals(team.getName())){
                if(eventList.get(index).getGoalsHome() < eventList.get(index).getGoalsAway()){
                    //condition vérifié
                    return seriesDefeat(straightWins, currentSeries+1, eventList, index+1, team);
                }else{
                    //condition pas vérifié
                    return false;
                }
                // cas de vérification possible
            }else {
                if(eventList.get(index).getGoalsHome() > eventList.get(index).getGoalsAway()){
                    //condition vérifié
                    return seriesDefeat(straightWins, currentSeries+1, eventList, index+1, team);
                }else{
                    //condition pas vérifié
                    return false;
                }
                // cas de vérification possible
            }
        }
    }

    @GetMapping (value = "/analyseVictory")
    @ResponseBody
    public List<AnalyseByDefeatBean> analyzeVictory(){
        List<Team> teamList = teamRepository.findAll();
        Double win = new Double(0);
        Integer potential = 0;
        Double moneyEarned = new Double(0);
        List<AnalyseByDefeatBean> analyseByDefeatBeans = new ArrayList<AnalyseByDefeatBean>();
        List<AnalyseByDefeatBean> results = new ArrayList<AnalyseByDefeatBean>();
        int maxEventListSize = 0;
        for(Team team : teamList){
            //eventList.addAll(eventRepository.findByTeamAwayEntityOrTeamHomeEntityOrderByDate(team, team));
            //eventRepository.save(new Event());
            List<Event> eventList = eventRepository.findByTeamAwayEntityOrTeamHomeEntityOrderByDate(team, team);
            if(eventList.size() > maxEventListSize){
                maxEventListSize = eventList.size();
            }
            for(int straightWins = 1; straightWins < eventList.size()-1; straightWins++) {
                win = new Double(0);
                potential = 0;
                moneyEarned = new Double(0);
                for (int i = 0; i < eventList.size() - straightWins; i++) {
                    if (seriesWin(straightWins, 0, eventList, i, team)) {
                        win++;
                        if(eventList.get(straightWins).getTeamHomeEntity() == team){
                            if(eventList.get(straightWins).getOddHome() != null){
                                moneyEarned += eventList.get(straightWins).getOddHome();
                            }else{
                                moneyEarned++;
                            }
                        }else{
                            if(eventList.get(straightWins).getOddAway() != null){
                                moneyEarned += eventList.get(straightWins).getOddAway();
                            }else{
                                moneyEarned++;
                            }
                        }
                    }
                }
                for (int i = 0; i < eventList.size() - straightWins; i++) {
                    if (seriesDefeat(straightWins - 1, 0, eventList, i, team)) {
                        potential++;
                    }
                }
                if(potential != 0){
                    analyseByDefeatBeans.add(new AnalyseByDefeatBean(100*win/potential, straightWins, potential, moneyEarned-potential));
                }
            }
        }

        for(int i = 0; i < maxEventListSize; i++){
            final int iFinal = i;
            List<AnalyseByDefeatBean> analyseByDefeatBeansByStraightWins = analyseByDefeatBeans.stream().filter(bean -> bean.getStraightDefeats() == iFinal).collect(Collectors.toList());
            Double average = new Double(0);
            Integer nbCandidate = 0;
            Double profitability = new Double(0);
            for(AnalyseByDefeatBean analyseByDefeatBean : analyseByDefeatBeansByStraightWins){
                average += analyseByDefeatBean.getPercentWins()*analyseByDefeatBean.getNbCandidate();
                nbCandidate += analyseByDefeatBean.getNbCandidate();
                profitability += analyseByDefeatBean.getProfitability();
            }
            average = average/nbCandidate;
            results.add(new AnalyseByDefeatBean(average, i, nbCandidate, 100*(profitability)/nbCandidate));
        }
        return results;
    }

    public boolean seriesWin(int straightWins, int currentSeries, List<Event> eventList, int index, Team team){
        if(currentSeries == straightWins){
            if(eventList.get(index).getTeamHomeName().equals(team.getName())){
                if(eventList.get(index).getGoalsHome() > eventList.get(index).getGoalsAway()){
                    //condition vérifié
                    return true;
                }else{
                    //condition pas vérifié
                    return false;
                }
                // cas de vérification possible
            }else {
                if(eventList.get(index).getGoalsHome() < eventList.get(index).getGoalsAway()){
                    //condition vérifié
                    return true;
                }else{
                    //condition pas vérifié
                    return false;
                }
                // cas de vérification possible
            }
        }else {
            if(eventList.get(index).getTeamHomeName().equals(team.getName())){
                if(eventList.get(index).getGoalsHome() < eventList.get(index).getGoalsAway()){
                    //condition vérifié
                    return seriesWin(straightWins, currentSeries+1, eventList, index+1, team);
                }else{
                    //condition pas vérifié
                    return false;
                }
                // cas de vérification possible
            }else {
                if(eventList.get(index).getGoalsHome() > eventList.get(index).getGoalsAway()){
                    //condition vérifié
                    return seriesWin(straightWins, currentSeries+1, eventList, index+1, team);
                }else{
                    //condition pas vérifié
                    return false;
                }
                // cas de vérification possible
            }
        }
    }

    @GetMapping (value = "/analyseDraw")
    @ResponseBody
    public List<AnalyseByDefeatBean> analyzeDraw(){
        List<Team> teamList = teamRepository.findAll();
        Double draw = new Double(0);
        Integer potential = 0;
        List<AnalyseByDefeatBean> analyseByDefeatBeans = new ArrayList<AnalyseByDefeatBean>();
        List<AnalyseByDefeatBean> results = new ArrayList<AnalyseByDefeatBean>();
        int maxEventListSize = 0;
        Double moneyEarned = new Double(0);
        for(Team team : teamList){
            //eventList.addAll(eventRepository.findByTeamAwayEntityOrTeamHomeEntityOrderByDate(team, team));
            //eventRepository.save(new Event());
            List<Event> eventList = eventRepository.findByTeamAwayEntityOrTeamHomeEntityOrderByDate(team, team);
            if(eventList.size() > maxEventListSize){
                maxEventListSize = eventList.size();
            }
            for(int straightWins = 1; straightWins < eventList.size()-1; straightWins++) {
                draw = new Double(0);
                potential = 0;
                moneyEarned = new Double(0);
                for (int i = 0; i < eventList.size() - straightWins; i++) {
                    if (seriesDraw(straightWins, 0, eventList, i, team)) {
                        draw++;
                        if(eventList.get(straightWins).getOddDraw() != null){
                            moneyEarned += eventList.get(straightWins).getOddDraw();
                        }else{
                            moneyEarned++;
                        }
                    }
                }
                for (int i = 0; i < eventList.size() - straightWins; i++) {
                    if (seriesDefeat(straightWins - 1, 0, eventList, i, team)) {
                        potential++;
                    }
                }
                if(potential != 0){
                    analyseByDefeatBeans.add(new AnalyseByDefeatBean(100*draw/potential, straightWins, potential, moneyEarned-potential));
                }
            }
        }

        for(int i = 0; i < maxEventListSize; i++){
            final int iFinal = i;
            List<AnalyseByDefeatBean> analyseByDefeatBeansByStraightWins = analyseByDefeatBeans.stream().filter(bean -> bean.getStraightDefeats() == iFinal).collect(Collectors.toList());
            Double average = new Double(0);
            Integer nbCandidate = 0;
            Double profitability = new Double(0);
            for(AnalyseByDefeatBean analyseByDefeatBean : analyseByDefeatBeansByStraightWins){
                average += analyseByDefeatBean.getPercentWins()*analyseByDefeatBean.getNbCandidate();
                nbCandidate += analyseByDefeatBean.getNbCandidate();
                profitability += analyseByDefeatBean.getProfitability();
            }
            average = average/nbCandidate;
            results.add(new AnalyseByDefeatBean(average, i, nbCandidate, 100*(profitability)/nbCandidate));
        }
        return results;
    }

    public boolean seriesDraw(int straightWins, int currentSeries, List<Event> eventList, int index, Team team){
        if(currentSeries == straightWins){
            if(eventList.get(index).getGoalsHome() == eventList.get(index).getGoalsAway()){
                //condition vérifié
                return true;
            }else{
                //condition pas vérifié
                return false;
            }
            // cas de vérification possible
        }else {
            if(eventList.get(index).getTeamHomeName().equals(team.getName())){
                if(eventList.get(index).getGoalsHome() < eventList.get(index).getGoalsAway()){
                    //condition vérifié
                    return seriesDraw(straightWins, currentSeries+1, eventList, index+1, team);
                }else{
                    //condition pas vérifié
                    return false;
                }
                // cas de vérification possible
            }else {
                if(eventList.get(index).getGoalsHome() > eventList.get(index).getGoalsAway()){
                    //condition vérifié
                    return seriesDraw(straightWins, currentSeries+1, eventList, index+1, team);
                }else{
                    //condition pas vérifié
                    return false;
                }
                // cas de vérification possible
            }
        }
    }
}
