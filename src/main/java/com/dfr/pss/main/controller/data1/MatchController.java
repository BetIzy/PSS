package com.dfr.pss.main.controller.data1;

import com.dfr.pss.main.entity.data1.Match;
import com.dfr.pss.main.repository.data1.MatchRepository;
import com.dfr.pss.main.entity.main.Competition;
import com.dfr.pss.main.entity.main.Event;
import com.dfr.pss.main.repository.main.CompetitionRepository;
import com.dfr.pss.main.repository.main.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@RequestMapping("/db_tmp/match")
@Controller
public class MatchController {

    @Autowired
    MatchRepository matchRepository;


    @Autowired
    CompetitionRepository competitionRepository;

    @Autowired
    EventRepository eventRepository;


    @RequestMapping("/getAll")
    @ResponseBody
    public List<Match> getAll() {
        return matchRepository.findAll();
    }

    @RequestMapping("/fillCompetition")
    @ResponseBody
    public List<String> updateLeague(@RequestBody List<Match> matchs){
        return saveCompetition(matchs);
    }

    private List<String> saveCompetition(List<Match> matchs){
        List<String> competitionReturns = new ArrayList<>();
        Competition competitionReturn = null;
        Competition checkUnique = null;
        for(Match match : matchs){
            checkUnique = competitionRepository.findByNameAndSeason(match.getLeague().getName(), match.getSeason());
            if(checkUnique == null) {
                Competition competition = new Competition();
                competition.setName(match.getLeague().getName());
                competition.setSeason(match.getSeason());
                competition.setId(match.getLeague().getId());
                competitionReturn = competitionRepository.save(competition);
                if (competitionReturn != null) {
                    competitionReturns.add(competitionReturn.getName() + " " + competitionReturn.getSeason());
                } else {
                    competitionReturns.add("");
                }
            }
        }
        return competitionReturns;
    }

    @RequestMapping("/fillEvent")
    @ResponseBody
    public List<String> updateEvent(@RequestBody List<Match> matchs){
        return saveEvent(matchs);
    }

    private List<String> saveEvent(List<Match> matchs){
        List<String> eventReturns = new ArrayList<>();
        Event eventReturn = null;
        for(Match match : matchs){
            Event event = eventRepository.findByTeamHomeNameAndTeamAwayNameAndDate(match.getHomeTeam().getLongName(), match.getAwayTeam().getLongName(), match.getDate());
            if(event == null) {
                event = new Event();
                event.setDate(match.getDate());
                event.setGoalsHome(match.getHomeGoal());
                event.setGoalsAway(match.getAwayGoal());
                event.setTeamHomeName(match.getHomeTeam().getLongName());
                event.setTeamAwayName(match.getAwayTeam().getLongName());
                event.setCompetitionName(match.getLeague().getName());
                event.setCompetitionSeason(match.getSeason());
                eventReturn = eventRepository.save(event);
                if (eventReturn != null) {
                    eventReturns.add(eventReturn.getDate().toString() + " " + eventReturn.getTeamHomeName() + "-" + eventReturn.getTeamAwayName());
                } else {
                    eventReturns.add("");
                }
            }
        }
        return  eventReturns;
    }
}
