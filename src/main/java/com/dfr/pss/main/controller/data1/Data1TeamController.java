package com.dfr.pss.main.controller.data1;

import com.dfr.pss.main.entity.data1.Data1Team;
import com.dfr.pss.main.repository.data1.Data1TeamRepository;
import com.dfr.pss.main.repository.main.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@RequestMapping("/db_tmp/team")
@Controller
public class Data1TeamController {


    @Autowired
    Data1TeamRepository data1TeamRepository;

    @Autowired
    TeamRepository teamRepository;


    @RequestMapping(value = "/getAll")
    @ResponseBody
    public List<Data1Team> getAll(){
        return data1TeamRepository.findAll();
    }


    @RequestMapping("/fillTeam")
    @ResponseBody
    public List<String> updateTeam(@RequestBody List<Data1Team> teams){
        return saveTeam(teams);
    }

    private List<String> saveTeam(List<Data1Team> teams){
        List<String> teamReturns = new ArrayList<>();
        com.dfr.pss.main.entity.main.Team teamReturn = null;
        com.dfr.pss.main.entity.main.Team checkUnique = null;
        for(Data1Team team : teams){
            checkUnique = teamRepository.findByName(team.getLongName());
            if(checkUnique == null){
                com.dfr.pss.main.entity.main.Team teamToCreate = new com.dfr.pss.main.entity.main.Team();
                teamToCreate.setName(team.getLongName());
                teamToCreate.setShortName(team.getShortName());
                teamReturn = teamRepository.save(teamToCreate);
                if (teamReturn != null) {
                    teamReturns.add(teamReturn.getName() + " " + teamReturn.getShortName());
                } else {
                    teamReturns.add("");
                }
            }
        }
        return teamReturns;
    }
}
