package com.dfr.pss.main.controller.main;

import com.dfr.pss.main.entity.data1.Data1Team;
import com.dfr.pss.main.entity.data1.Match;
import com.dfr.pss.main.entity.main.Event;
import com.dfr.pss.main.repository.data1.Data1TeamRepository;
import com.dfr.pss.main.repository.data1.MatchRepository;
import com.dfr.pss.main.repository.main.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.persistence.NonUniqueResultException;
import java.util.ArrayList;
import java.util.List;

@RequestMapping("/event")
@Controller
public class EventController {

    @Autowired
    EventRepository eventRepository;

    @Autowired
    MatchRepository matchRepository;

    @Autowired
    Data1TeamRepository data1TeamRepository;

    @RequestMapping(value = "/getAll")
    @ResponseBody
    public List<Event> getAll(){ return eventRepository.findAll();}

    @PostMapping(value = "updateOdd")
    @ResponseBody
    public List<String> updateOdd(){
        List<Event> events = eventRepository.findAll();
        return saveOdd(events);
    }

    private List<String> saveOdd(List<Event> events){
        List<String> eventReturns = new ArrayList<String>();
        Event eventReturn = null;
        for(Event event : events){
            Data1Team homeTeam = null;
            Data1Team awayTeam = null;
            try{
               homeTeam = data1TeamRepository.findByLongName(event.getTeamHomeName());
            }catch (Exception exception){
                System.out.println("TODO : DELETE "+event.getTeamHomeName());
            }
            try{
                awayTeam = data1TeamRepository.findByLongName(event.getTeamAwayName());
            }catch (Exception exception){
                System.out.println("TODO : DELETE "+event.getTeamAwayName());
            }
            if(homeTeam != null && awayTeam != null) {
                Match match = matchRepository.findByDateAndAwayTeamIdAndHomeTeamId(event.getDate(), awayTeam.getTeam_api_id(), homeTeam.getTeam_api_id());
                if (match != null) {
                    event.setOddHome(match.getOddHome());
                    event.setOddDraw(match.getOddDraw());
                    event.setOddAway(match.getOddAway());
                    eventRepository.save(event);
                    if (eventReturn != null) {
                        eventReturns.add(eventReturn.getDate().toString() + " " + eventReturn.getTeamHomeName() + "-" + eventReturn.getTeamAwayName());
                    } else {
                        eventReturns.add("");
                    }
                }
            }
        }
        return eventReturns;
    }

    @PostMapping(value = "/updateAll")
    @ResponseBody
    public List<String> updateEvent(@RequestBody List<Event> events){return saveEvent(events);}

    private List<String> saveEvent(List<Event> events){
        List<String> eventReturns = new ArrayList<String>();
        Event eventReturn = null;
        for(Event event : events){
            eventReturn = eventRepository.save(event);
            if(eventReturn != null){
                eventReturns.add(eventReturn.getDate().toString() + " " + eventReturn.getTeamHomeName() + "-" + eventReturn.getTeamAwayName());
            }else{
                eventReturns.add("");
            }
        }
        return eventReturns;
    }
}
