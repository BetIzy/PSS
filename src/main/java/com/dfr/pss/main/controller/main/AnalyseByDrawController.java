package com.dfr.pss.main.controller.main;

import com.dfr.pss.main.bean.main.AnalyseByDrawBean;
import com.dfr.pss.main.entity.main.Event;
import com.dfr.pss.main.entity.main.Team;
import com.dfr.pss.main.repository.main.EventRepository;
import com.dfr.pss.main.repository.main.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RequestMapping("/analysisByDraw")
@Controller
public class AnalyseByDrawController {

    @Autowired
    TeamRepository teamRepository;

    @Autowired
    EventRepository eventRepository;

    @GetMapping (value = "/analyseDefeat")
    @ResponseBody
    public List<AnalyseByDrawBean> analyzeDefeat(){
        List<Team> teamList = teamRepository.findAll();
        Double defeat = new Double(0);
        Integer potential = 0;
        List<AnalyseByDrawBean> analyseByDrawBeans = new ArrayList<AnalyseByDrawBean>();
        List<AnalyseByDrawBean> results = new ArrayList<AnalyseByDrawBean>();
        int maxEventListSize = 0;
        for(Team team : teamList){
            //eventList.addAll(eventRepository.findByTeamAwayEntityOrTeamHomeEntityOrderByDate(team, team));
            //eventRepository.save(new Event());
            List<Event> eventList = eventRepository.findByTeamAwayEntityOrTeamHomeEntityOrderByDate(team, team);
            if(eventList.size() > maxEventListSize){
                maxEventListSize = eventList.size();
            }
            for(int straightDraws = 1; straightDraws < eventList.size()-1; straightDraws++) {
                defeat = new Double(0);
                potential = 0;
                for (int i = 0; i < eventList.size() - straightDraws; i++) {
                    if (seriesDefeat(straightDraws, 0, eventList, i, team)) {
                        defeat++;
                    }
                }
                for (int i = 0; i < eventList.size() - straightDraws; i++) {
                    if (seriesDraw(straightDraws - 1, 0, eventList, i, team)) {
                        potential++;
                    }
                }
                if(potential != 0){
                    analyseByDrawBeans.add(new AnalyseByDrawBean(100*defeat/potential, straightDraws, potential));
                }
            }
        }

        for(int i = 0; i < maxEventListSize; i++){
            final int iFinal = i;
            List<AnalyseByDrawBean> analyseByDrawBeansByStraightDraws = analyseByDrawBeans.stream().filter(bean -> bean.getStraightDraws() == iFinal).collect(Collectors.toList());
            Double average = new Double(0);
            Integer nbCandidate = 0;
            for(AnalyseByDrawBean analyseByDrawBean : analyseByDrawBeansByStraightDraws){
                average += analyseByDrawBean.getPercentWins()*analyseByDrawBean.getNbCandidate();
                nbCandidate += analyseByDrawBean.getNbCandidate();
            }
            average = average/nbCandidate;
            results.add(new AnalyseByDrawBean(average, i, nbCandidate));
        }
        return results;
    }

    public boolean seriesDefeat(int straightDraws, int currentSeries, List<Event> eventList, int index, Team team){
        if(currentSeries == straightDraws){
            if(eventList.get(index).getTeamHomeName().equals(team.getName())){
                if(eventList.get(index).getGoalsHome() < eventList.get(index).getGoalsAway()){
                    //condition vérifié
                    return true;
                }else{
                    //condition pas vérifié
                    return false;
                }
                // cas de vérification possible
            }else {
                if(eventList.get(index).getGoalsHome() > eventList.get(index).getGoalsAway()){
                    //condition vérifié
                    return true;
                }else{
                    //condition pas vérifié
                    return false;
                }
                // cas de vérification possible
            }
        }else {
            if(eventList.get(index).getGoalsHome() == eventList.get(index).getGoalsAway()){
                //condition vérifié
                return seriesDefeat(straightDraws, currentSeries+1, eventList, index+1, team);
            }else{
                //condition pas vérifié
                return false;
            }
        }
    }

    @GetMapping (value = "/analyseVictory")
    @ResponseBody
    public List<AnalyseByDrawBean> analyzeVictory(){
        // On récupère toutes les équipes
        List<Team> teamList = teamRepository.findAll();
        Double win = new Double(0);
        Integer potential = 0;
        List<AnalyseByDrawBean> analyseByDrawBeans = new ArrayList<AnalyseByDrawBean>();
        List<AnalyseByDrawBean> results = new ArrayList<AnalyseByDrawBean>();
        int maxEventListSize = 0;
        // Pour chaque équipe
        for(Team team : teamList){
            // On récupère tous les matchs dans lesquels l'équipe est présente
            List<Event> eventList = eventRepository.findByTeamAwayEntityOrTeamHomeEntityOrderByDate(team, team);
            if(eventList.size() > maxEventListSize){
                maxEventListSize = eventList.size();
            }
            // straightWins -> Série de victoire pour avoir une équipe potentielle

            for(int straightDraws = 1; straightDraws < eventList.size()-1; straightDraws++) {
                win = new Double(0);
                potential = 0;
                for (int i = 0; i < eventList.size() - straightDraws; i++) {
                    // Si l'équipe gagne, on incrémente currentSeries et i
                    // Si currentSeries == straightWins et que l'équipe gagne on a une équipe remplissant la condition de victoire après séries de victoire
                    // Pour starightWins = 1 on cherche les équipes à deux victoires d'affilées
                    if (seriesWin(straightDraws, 0, eventList, i, team)) {
                        win++;
                    }
                }
                for (int i = 0; i < eventList.size() - straightDraws; i++) {
                    // Si l'équipe gagne, on incrémente currentSeries et i
                    // Si currentSeries == straightWins et que l'équipe gagne on a une équipe remplissant la condition de séries de victoire
                    if (seriesDraw(straightDraws - 1, 0, eventList, i, team)) {
                        potential++;
                    }
                }
                if(potential != 0){
                    analyseByDrawBeans.add(new AnalyseByDrawBean(100*win/potential, straightDraws, potential));
                }
            }
        }

        for(int i = 0; i < maxEventListSize; i++){
            final int iFinal = i;
            List<AnalyseByDrawBean> analyseByDrawBeansByStraightDraws = analyseByDrawBeans.stream().filter(bean -> bean.getStraightDraws() == iFinal).collect(Collectors.toList());
            Double average = new Double(0);
            Integer nbCandidate = 0;
            for(AnalyseByDrawBean analyseByDrawBean : analyseByDrawBeansByStraightDraws){
                average += analyseByDrawBean.getPercentWins()*analyseByDrawBean.getNbCandidate();
                nbCandidate += analyseByDrawBean.getNbCandidate();
            }
            average = average/nbCandidate;
            results.add(new AnalyseByDrawBean(average, i, nbCandidate));
        }
        return results;
    }

    public boolean seriesWin(int straightDraws, int currentSeries, List<Event> eventList, int index, Team team){
        if(currentSeries == straightDraws){
            if(eventList.get(index).getTeamHomeName().equals(team.getName())){
                if(eventList.get(index).getGoalsHome() > eventList.get(index).getGoalsAway()){
                    //condition vérifié
                    return true;
                }else{
                    //condition pas vérifié
                    return false;
                }
                // cas de vérification possible
            }else {
                if(eventList.get(index).getGoalsHome() < eventList.get(index).getGoalsAway()){
                    //condition vérifié
                    return true;
                }else{
                    //condition pas vérifié
                    return false;
                }
                // cas de vérification possible
            }
        }else {
            if(eventList.get(index).getGoalsHome() == eventList.get(index).getGoalsAway()){
                //condition vérifié
                return seriesWin(straightDraws, currentSeries+1, eventList, index+1, team);
            }else{
                //condition pas vérifié
                return false;
            }
        }
    }

    @GetMapping (value = "/analyseDraw")
    @ResponseBody
    public List<AnalyseByDrawBean> analyzeDraw(){
        List<Team> teamList = teamRepository.findAll();
        Double draw = new Double(0);
        Integer potential = 0;
        List<AnalyseByDrawBean> analyseByDrawBeans = new ArrayList<AnalyseByDrawBean>();
        List<AnalyseByDrawBean> results = new ArrayList<AnalyseByDrawBean>();
        int maxEventListSize = 0;
        for(Team team : teamList){
            //eventList.addAll(eventRepository.findByTeamAwayEntityOrTeamHomeEntityOrderByDate(team, team));
            //eventRepository.save(new Event());
            List<Event> eventList = eventRepository.findByTeamAwayEntityOrTeamHomeEntityOrderByDate(team, team);
            if(eventList.size() > maxEventListSize){
                maxEventListSize = eventList.size();
            }
            for(int straightDraws = 1; straightDraws < eventList.size()-1; straightDraws++) {
                draw = new Double(0);
                potential = 0;
                for (int i = 0; i < eventList.size() - straightDraws; i++) {
                    if (seriesDraw(straightDraws, 0, eventList, i, team)) {
                        draw++;
                    }
                }
                for (int i = 0; i < eventList.size() - straightDraws; i++) {
                    if (seriesDraw(straightDraws - 1, 0, eventList, i, team)) {
                        potential++;
                    }
                }
                if(potential != 0){
                    analyseByDrawBeans.add(new AnalyseByDrawBean(100*draw/potential, straightDraws, potential));
                }
            }
        }

        for(int i = 0; i < maxEventListSize; i++){
            final int iFinal = i;
            List<AnalyseByDrawBean> analyseByDrawBeansByStraightDraws = analyseByDrawBeans.stream().filter(bean -> bean.getStraightDraws() == iFinal).collect(Collectors.toList());
            Double average = new Double(0);
            Integer nbCandidate = 0;
            for(AnalyseByDrawBean analyseByDrawBean : analyseByDrawBeansByStraightDraws){
                average += analyseByDrawBean.getPercentWins()*analyseByDrawBean.getNbCandidate();
                nbCandidate += analyseByDrawBean.getNbCandidate();
            }
            average = average/nbCandidate;
            results.add(new AnalyseByDrawBean(average, i, nbCandidate));
        }
        return results;
    }

    public boolean seriesDraw(int straightWins, int currentSeries, List<Event> eventList, int index, Team team){
        if(currentSeries == straightWins){
            if(eventList.get(index).getGoalsHome() == eventList.get(index).getGoalsAway()){
                //condition vérifié
                return true;
            }else{
                //condition pas vérifié
                return false;
            }
        }else {
                if(eventList.get(index).getGoalsHome() == eventList.get(index).getGoalsAway()){
                    //condition vérifié
                    return seriesDraw(straightWins, currentSeries+1, eventList, index+1, team);
                }else{
                    //condition pas vérifié
                    return false;
                }
        }
    }
}
