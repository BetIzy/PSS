package com.dfr.pss.main.controller.main;

import com.dfr.pss.main.entity.main.Sport;
import com.dfr.pss.main.repository.main.SportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@RequestMapping("/sport")
@Controller
public class SportController {

    @Autowired
    SportRepository sportRepository;

    @RequestMapping (value="/create")
    @ResponseBody
    public int createSport(@RequestBody Sport sport){
        return saveSport(sport);
    }

    private int saveSport(Sport sport){
        Sport sportReturn = sportRepository.save(sport);
        if(sportReturn != null) {
            return sportReturn.getId();
        }else{
            return -1;
        }
    }


}
