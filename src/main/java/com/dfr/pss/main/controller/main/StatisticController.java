package com.dfr.pss.main.controller.main;

import com.dfr.pss.main.entity.main.Competition;
import com.dfr.pss.main.entity.main.Event;
import com.dfr.pss.main.entity.main.Statistic;
import com.dfr.pss.main.entity.main.Team;
import com.dfr.pss.main.repository.main.CompetitionRepository;
import com.dfr.pss.main.repository.main.EventRepository;
import com.dfr.pss.main.repository.main.StatisticRepository;
import com.dfr.pss.main.repository.main.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@RequestMapping("/statistic")
@Controller
public class StatisticController {

    @Autowired
    StatisticRepository statisticRepository;

    @Autowired
    TeamRepository teamRepository;

    @Autowired
    EventRepository eventRepository;

    @Autowired
    CompetitionRepository competitionRepository;

    /*@PostMapping(value = "/updateAll")
    @ResponseBody
    public List<String> updateStatistic(@RequestBody List<Statistic> statistics){
        return saveStatistic(statistics);
    }

    private List<String> saveStatistic(List<Statistic> statistics){
        List<String> statisticReturns = new ArrayList<String>();
        Statistic statisticReturn = null;
        for(Statistic statistic : statistics){
            statisticReturn = statisticRepository.save(statistic);
            if(statisticReturn != null){
                statisticReturns.add(statisticReturn.getCompetitionId() + " " + statisticReturn.getTeamName());
            }else{
                statisticReturns.add("");
            }
        }
        return statisticReturns;
    }*/

    @PostMapping(value = "/updateAll")
    @ResponseBody
    public List<String> updateStatistic(){
        return saveStatistics();
    }

    private List<String> saveStatistics(){
        List<String> statisticReturns = new ArrayList<>();
        Statistic statisticReturn = null;
        Integer goalsHome;
        Integer goalsAway;
        Integer winsHome;
        Integer winsAway;
        Integer lossesHome;
        Integer lossesAway;
        Integer drawsHome;
        Integer drawsAway;
        List<Team> teams = new ArrayList<>();
        List<Event> events = new ArrayList<>();
        List<Competition> competitions = new ArrayList<>();
        competitions = competitionRepository.findAll();
        for(Competition competition : competitions) {

            teams = teamRepository.findAll();

            for (Team team : teams) {
                goalsHome = 0;
                goalsAway = 0;
                winsHome = 0;
                winsAway = 0;
                lossesHome = 0;
                lossesAway = 0;
                drawsHome = 0;
                drawsAway = 0;
                events = eventRepository.findByCompetitionAndTeamAwayEntityOrTeamHomeEntityOrderByDate(competition, team, team);
                for (Event event : events) {
                    if (event.getTeamHomeName().equals(team.getName())) {
                        if (event.getGoalsHome() > event.getGoalsAway()) {
                            winsHome++;
                        } else if (event.getGoalsHome() < event.getGoalsAway()) {
                            lossesHome++;
                        } else {
                            drawsHome++;
                        }
                        goalsHome += event.getGoalsHome();
                    } else {
                        if (event.getGoalsHome() < event.getGoalsAway()) {
                            winsAway++;
                        } else if (event.getGoalsHome() > event.getGoalsAway()) {
                            lossesAway++;
                        } else {
                            drawsAway++;
                        }
                        goalsAway += event.getGoalsAway();
                    }
                    Statistic statistic = statisticRepository.findByDateAndTeamNameAndCompetitionId(event.getDate(), team.getName(), competition.getId());
                    if(statistic == null){
                        statistic = new Statistic();
                        statistic.setCompetitionEntity(competition);
                        statistic.setCompetitionId(competition.getId());
                        statistic.setCompetitionName(competition.getName());
                        statistic.setCompetitionSeason(competition.getSeason());
                        statistic.setDrawsAway(drawsAway);
                        statistic.setDrawsHome(drawsHome);
                        statistic.setGoalsAway(goalsAway);
                        statistic.setGoalsHome(goalsHome);
                        statistic.setLossesAway(lossesAway);
                        statistic.setLossesHome(lossesHome);
                        statistic.setTeamEntity(team);
                        statistic.setTeamName(team.getName());
                        statistic.setWinsAway(winsAway);
                        statistic.setWinsHome(winsHome);
                        statistic.setDate(event.getDate());
                        statisticReturn = statisticRepository.save(statistic);
                        if(statisticReturn != null){
                            statisticReturns.add(statisticReturn.getCompetitionId() + " " + statisticReturn.getTeamName());
                        }else{
                            statisticReturns.add("");
                        }
                    }
                }
            }
        }

        return statisticReturns;
    }

}
