package com.dfr.pss.main.controller.main;

import com.dfr.pss.main.entity.main.Team;
import com.dfr.pss.main.repository.main.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@RequestMapping("/team")
@Controller
public class TeamController {

    @Autowired
    TeamRepository teamRepository;


    @PostMapping(value = "/updateAll")
    @ResponseBody
    public List<String> updateTeam(@RequestBody List<Team> teams){
        return saveTeam(teams);
    }

    private List<String> saveTeam(List<Team> teams){
        List<String> teamReturns = new ArrayList<String>();
        Team teamReturn = null;
        for(Team team : teams){
            teamReturn = teamRepository.save(team);
            if(teamReturn != null){
                teamReturns.add(teamReturn.getName());
            }else{
                teamReturns.add("");
            }
        }
        return teamReturns;
    }


}
