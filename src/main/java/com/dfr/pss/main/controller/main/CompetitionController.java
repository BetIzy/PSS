package com.dfr.pss.main.controller.main;

import com.dfr.pss.main.entity.main.Competition;
import com.dfr.pss.main.repository.main.CompetitionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@RequestMapping("/competition")
@Controller
public class CompetitionController {

    @Autowired
    CompetitionRepository competitionRepository;

    @RequestMapping(value = "/getAll")
    @ResponseBody
    public List<Competition> getAll(){
        return competitionRepository.findAll();
    }

    @PostMapping (value = "/updateAll")
    @ResponseBody
    public List<Integer> updateCompetition(@RequestBody List<Competition> competitions){
        return saveCompetition(competitions);
    }

    public Competition getCompetitionById(Integer id){
        return competitionRepository.findByCompetitionId(id);
    }

    private List<Integer> saveCompetition(List<Competition> competitions){
        List<Integer> competitionReturns = new ArrayList<Integer>();
        Competition competitionReturn = null;
        for(Competition competition : competitions){
            competitionReturn = competitionRepository.save(competition);
            if(competitionReturn != null){
                competitionReturns.add(competitionReturn.getId());
            }else{
                competitionReturns.add(-1);
            }
        }
        return competitionReturns;

    }

}
