package com.dfr.pss.main.controller.data1;

import com.dfr.pss.main.entity.data1.League;
import com.dfr.pss.main.repository.data1.LeagueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@RequestMapping("/db_tmp/league")
@Controller
public class LeagueController {

    @Autowired
    LeagueRepository leagueRepository;


    @RequestMapping("/getAll")
    @ResponseBody
    public List<League> getAll(){
        return leagueRepository.findAll();
    }



}
