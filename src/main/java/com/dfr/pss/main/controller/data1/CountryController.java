package com.dfr.pss.main.controller.data1;

import com.dfr.pss.main.entity.data1.Country;
import com.dfr.pss.main.repository.data1.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class CountryController {

    @Autowired
    CountryRepository countryRepository;

    @RequestMapping("/db_tmp/{id}")
    @ResponseBody
    public String db(@PathVariable("id") Long id){
        Country country = countryRepository.findById(id);

        return country.getName();
    }
}
