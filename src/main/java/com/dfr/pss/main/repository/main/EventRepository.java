package com.dfr.pss.main.repository.main;

import com.dfr.pss.main.entity.main.Competition;
import com.dfr.pss.main.entity.main.Event;
import com.dfr.pss.main.entity.main.Team;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

public interface EventRepository extends CrudRepository<Event, Integer> {

    public Event save(Event event);

    public List<Event> findAll();

    public List<Event> findByTeamAwayEntityOrTeamHomeEntityOrderByDate(Team teamHomeEntity, Team teamAwayEntity);

    public Event findByTeamHomeNameAndTeamAwayNameAndDate(String teamHomeName, String teamAwayEntity, Date date);

    @Query("SELECT e FROM Event e LEFT JOIN FETCH e.competition LEFT JOIN e.teamAwayEntity LEFT JOIN e.teamHomeEntity WHERE e.competition = ?1 AND (e.teamHomeEntity = ?2 OR e.teamAwayEntity = ?3)")
    public List<Event> findByCompetitionAndTeamAwayEntityOrTeamHomeEntityOrderByDate(Competition competition, Team teamHomeEntity, Team teamAwayEntity);
    //public List<Event> findByCompetitionAndTeamAwayEntityOrderByDate(Competition competition, Team teamHomeEntity);
    //public List<Event> findByCompetitionOrderByDate(Competition competition);


}
