package com.dfr.pss.main.repository.data1;

import com.dfr.pss.main.entity.data1.Data1Team;
import com.dfr.pss.main.entity.data1.Data1Team;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface Data1TeamRepository extends CrudRepository<Data1Team, Integer> {

    public List<Data1Team> findAll();

    public Data1Team findByLongName(String name);

}
