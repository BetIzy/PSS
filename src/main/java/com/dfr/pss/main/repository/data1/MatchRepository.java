package com.dfr.pss.main.repository.data1;

import com.dfr.pss.main.entity.data1.Data1Team;
import com.dfr.pss.main.entity.data1.Match;
import com.dfr.pss.main.entity.main.Team;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

public interface MatchRepository extends CrudRepository<Match, Integer> {

    @Query("SELECT m FROM Match m LEFT JOIN FETCH m.league LEFT JOIN FETCH m.homeTeam LEFT JOIN FETCH m.awayTeam")
    public List<Match> findAll();

    public Match findByDateAndAwayTeamIdAndHomeTeamId(Date date, Integer awayTeam, Integer homeTeam);

    public Match findByDate(Date date);

}
