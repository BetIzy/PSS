package com.dfr.pss.main.repository.data1;

import com.dfr.pss.main.entity.data1.Country;
import org.springframework.data.repository.CrudRepository;

public interface CountryRepository extends CrudRepository<Country, Integer> {

    Country findById(Long id);

}

