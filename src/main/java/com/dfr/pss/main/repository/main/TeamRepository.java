package com.dfr.pss.main.repository.main;

import com.dfr.pss.main.entity.main.Team;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TeamRepository extends CrudRepository<Team, Integer> {

    public Team save(Team team);

    public Team findByName(String name);

    public List<Team> findAll();

}
