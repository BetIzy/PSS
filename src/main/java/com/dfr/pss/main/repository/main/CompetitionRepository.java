package com.dfr.pss.main.repository.main;

import com.dfr.pss.main.entity.main.Competition;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface CompetitionRepository extends CrudRepository<Competition, Integer> {

    public Competition save(Competition competition);

    public List<Competition> findAll();

    public Competition findByNameAndSeason(String name, String season);

    public Competition findByCompetitionId(Integer competitionId);
}
