package com.dfr.pss.main.repository.main;

import com.dfr.pss.main.entity.main.Sport;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface SportRepository extends CrudRepository<Sport, Integer> {

    public Sport save(Sport sport);
}
