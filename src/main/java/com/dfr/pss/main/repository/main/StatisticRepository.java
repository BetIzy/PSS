package com.dfr.pss.main.repository.main;

import com.dfr.pss.main.entity.main.Statistic;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;

public interface StatisticRepository extends CrudRepository<Statistic, Integer> {
    public Statistic save(Statistic statistic);

    public Statistic findByDateAndTeamNameAndCompetitionId(Date date, String teamName, Integer competitionId);
}
