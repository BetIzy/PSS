package com.dfr.pss.main.repository.data1;

import com.dfr.pss.main.entity.data1.League;
import org.springframework.data.repository.CrudRepository;

import org.springframework.transaction.annotation.Transactional;


import java.util.List;

@Transactional
public interface LeagueRepository extends CrudRepository<League, Integer> {

    public List<League> findAll();

    public League findById(Integer id);
}
