package com.dfr.pss.main.bean.main;

public class AnalyseByDefeatBean {

    private Double percentWins;
    private int straightDefeats;
    private int nbCandidate;
    private Double profitability;

    public AnalyseByDefeatBean(Double percentWins, int straightDefeats, int nbCandidate, Double profitability){
        this.percentWins = percentWins;
        this.straightDefeats = straightDefeats;
        this.nbCandidate = nbCandidate;
        this.profitability = profitability;
    }

    public Double getProfitability() {
        return profitability;
    }

    public void setProfitability(Double profitability) {
        this.profitability = profitability;
    }

    public Double getPercentWins() {
        return percentWins;
    }

    public void setPercentWins(Double percentWins) {
        this.percentWins = percentWins;
    }

    public int getStraightDefeats() {
        return straightDefeats;
    }

    public void setStraightDefeats(int straightDefeats) {
        this.straightDefeats = straightDefeats;
    }

    public int getNbCandidate() {
        return nbCandidate;
    }

    public void setNbCandidate(int nbCandidate) {
        this.nbCandidate = nbCandidate;
    }
}
