package com.dfr.pss.main.bean.main;

public class AnalyseByDrawBean {

    private Double percentWins;
    private int straightDraws;
    private int nbCandidate;
    private Double profitability;

    public AnalyseByDrawBean(Double percentWins, int straightDraws, int nbCandidate/*, Double profability*/){
        this.percentWins = percentWins;
        this.straightDraws = straightDraws;
        this.nbCandidate = nbCandidate;
        //this.profitability = profability;
    }

    public Double getProfitability() {
        return profitability;
    }

    public void setProfitability(Double profitability) {
        this.profitability = profitability;
    }

    public Double getPercentWins() {
        return percentWins;
    }

    public void setPercentWins(Double percentWins) {
        this.percentWins = percentWins;
    }

    public int getStraightDraws() {
        return straightDraws;
    }

    public void setStraightDraws(int straightDraws) {
        this.straightDraws = straightDraws;
    }

    public int getNbCandidate() {
        return nbCandidate;
    }

    public void setNbCandidate(int nbCandidate) {
        this.nbCandidate = nbCandidate;
    }
}
