package com.dfr.pss.main.bean.main;

public class AnalyseByVictoryBean {

    private Double percentWins;
    private int straightWins;
    private int nbCandidate;
    private Double profitability;


    public AnalyseByVictoryBean(Double percentWins, int straightWins, int nbCandidate, Double profitability){
        this.percentWins = percentWins;
        this.straightWins = straightWins;
        this.nbCandidate = nbCandidate;
        this.profitability = profitability;

    }

    public Double getProfitability() {
        return profitability;
    }

    public void setProfitability(Double profitability) {
        this.profitability = profitability;
    }

    public Double getPercentWins() {
        return percentWins;
    }

    public void setPercentWins(Double percentWins) {
        this.percentWins = percentWins;
    }

    public int getStraightWins() {
        return straightWins;
    }

    public void setStraightWins(int straightWins) {
        this.straightWins = straightWins;
    }

    public int getNbCandidate() {
        return nbCandidate;
    }

    public void setNbCandidate(int nbCandidate) {
        this.nbCandidate = nbCandidate;
    }
}
