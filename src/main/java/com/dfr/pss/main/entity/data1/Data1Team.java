package com.dfr.pss.main.entity.data1;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "data1_team")
public class Data1Team implements Serializable{

    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "team_api_id")
    private Integer team_api_id;

    @Column(name = "team_long_name")
    private String longName;

    @Column(name = "team_short_name")
    private String shortName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTeam_api_id() {
        return team_api_id;
    }

    public void setTeam_api_id(Integer team_api_id) {
        this.team_api_id = team_api_id;
    }

    public String getLongName() {
        return longName;
    }

    public void setLongName(String longName) {
        this.longName = longName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }
}
