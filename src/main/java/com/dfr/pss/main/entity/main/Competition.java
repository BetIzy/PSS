package com.dfr.pss.main.entity.main;

import javax.persistence.*;
import java.util.List;

@Entity
@IdClass(CompetitionId.class)
@Table(name="main_competition")
public class Competition {

    @Id
    @Column(name = "competition_name", nullable = false, length = 45)
    private String name;

    @Id
    @Column(name = "competition_season", nullable = false, length = 45)
    private String season;

    @Column(name = "competition_id", nullable = false, length = 11)
    private Integer competitionId;

    @OneToMany(mappedBy = "competitionEntity")
    private List<Statistic> statistics;


    public List<Statistic> getStatistics() {
        return statistics;
    }

    public void setStatistics(List<Statistic> statistics) {
        this.statistics = statistics;
    }

    public Integer getId() {
        return competitionId;
    }

    public void setId(Integer competitionId) {
        this.competitionId = competitionId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }


}
