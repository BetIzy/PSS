package com.dfr.pss.main.entity.data1;


import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "data1_event")
public class Match {

    @Id
    @Column(name = "id")
    private Integer id;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "league_id")
    private League league;

    @Column(name = "season")
    private String season;

    @Column(name = "date")
    private Date date;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "home_team_api_id", referencedColumnName = "team_api_id")
    private Data1Team homeTeam;

    @Column(name = "home_team_api_id", updatable = false, insertable = false)
    private Integer homeTeamId;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "away_team_api_id", referencedColumnName = "team_api_id")
    private Data1Team awayTeam;

    @Column(name = "away_team_api_id", updatable = false, insertable = false)
    private Integer awayTeamId;

    @Column(name = "home_team_goal")
    private Integer homeGoal;

    @Column(name = "away_team_goal")
    private Integer awayGoal;

    @Column(name = "B365H")
    private Double oddHome;

    @Column(name = "B365D")
    private Double oddDraw;

    public Integer getHomeTeamId() {
        return homeTeamId;
    }

    public void setHomeTeamId(Integer homeTeamId) {
        this.homeTeamId = homeTeamId;
    }

    public Double getOddHome() {
        return oddHome;
    }

    public void setOddHome(Double oddHome) {
        this.oddHome = oddHome;
    }

    public Double getOddDraw() {
        return oddDraw;
    }

    public void setOddDraw(Double oddDraw) {
        this.oddDraw = oddDraw;
    }

    public Double getOddAway() {
        return oddAway;
    }

    public void setOddAway(Double oddAway) {
        this.oddAway = oddAway;
    }

    @Column(name = "B365A")
    private Double oddAway;

    public Integer getHomeGoal() {
        return homeGoal;
    }

    public void setHomeGoal(Integer homeGoal) {
        this.homeGoal = homeGoal;
    }

    public Integer getAwayGoal() {
        return awayGoal;
    }

    public void setAwayGoal(Integer awayGoal) {
        this.awayGoal = awayGoal;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public League getLeague() {
        return league;
    }

    public void setLeague(League league) {
        this.league = league;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Data1Team getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(Data1Team homeTeam) {
        this.homeTeam = homeTeam;
    }

    public Data1Team getAwayTeam() {
        return awayTeam;
    }

    public void setAwayTeam(Data1Team awayTeam) {
        this.awayTeam = awayTeam;
    }
}
