package com.dfr.pss.main.entity.main;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.Date;

@Entity
@IdClass(StatisticId.class)
@Table(name = "main_statistic")
public class Statistic {

    @Id
    @Column(name = "statistic_team_name", nullable = false, length = 45)
    private String teamName;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "statistic_team_name", insertable = false, updatable = false)
    @JsonIgnore
    private Team teamEntity;

    @Id
    @Column(name = "statistic_competition_id", nullable = false, length = 11)
    private Integer competitionId;

    @Id
    @Column(name = "statistic_date", nullable = false)
    private Date date;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name = "statistic_competition_name", insertable = false, updatable = false),
            @JoinColumn(name = "statistic_competition_season", insertable = false, updatable = false)
    })
    @JsonIgnore
    private Competition competitionEntity;

    @Column(name = "statistic_competition_name", nullable = false, length = 45)
    private String competitionName;

    @Column(name = "statistic_competition_season", nullable = false, length = 45)
    private String competitionSeason;

    @Column(name = "statistic_goals_home", length = 11)
    private Integer goalsHome;

    @Column(name = "statistic_goals_away", length = 11)
    private Integer goalsAway;

    @Column(name = "statistic_wins_away", length = 11)
    private Integer winsAway;

    @Column(name = "statistic_wins_home", length = 11)
    private Integer winsHome;

    @Column(name = "statistic_losses_home", length = 11)
    private Integer lossesHome;

    @Column(name = "statistic_losses_away", length = 11)
    private Integer lossesAway;

    @Column(name = "statistic_draws_home", length = 11)
    private Integer drawsHome;

    @Column(name = "statistic_draws_away", length = 11)
    private Integer drawsAway;

    @JsonIgnore
    public Competition getCompetitionEntity() {
        return competitionEntity;
    }

    @JsonProperty
    public void setCompetitionEntity(Competition competitionEntity) {
        this.competitionEntity = competitionEntity;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getCompetitionName() {
        return competitionName;
    }

    public void setCompetitionName(String competitionName) {
        this.competitionName = competitionName;
    }

    public String getCompetitionSeason() {
        return competitionSeason;
    }

    public void setCompetitionSeason(String competitionSeason) {
        this.competitionSeason = competitionSeason;
    }

    @JsonIgnore
    public Team getTeamEntity() {
        return teamEntity;
    }

    @JsonProperty
    public void setTeamEntity(Team teamEntity) {
        this.teamEntity = teamEntity;
    }

    public Integer getCompetitionId() {
        return competitionId;
    }

    public void setCompetitionId(Integer competitionId) {
        this.competitionId = competitionId;
    }

    public Integer getGoalsHome() {
        return goalsHome;
    }

    public void setGoalsHome(Integer goalsHome) {
        this.goalsHome = goalsHome;
    }

    public Integer getGoalsAway() {
        return goalsAway;
    }

    public void setGoalsAway(Integer goalsAway) {
        this.goalsAway = goalsAway;
    }

    public Integer getWinsAway() {
        return winsAway;
    }

    public void setWinsAway(Integer winsAway) {
        this.winsAway = winsAway;
    }

    public Integer getWinsHome() {
        return winsHome;
    }

    public void setWinsHome(Integer winsHome) {
        this.winsHome = winsHome;
    }

    public Integer getLossesHome() {
        return lossesHome;
    }

    public void setLossesHome(Integer lossesHome) {
        this.lossesHome = lossesHome;
    }

    public Integer getLossesAway() {
        return lossesAway;
    }

    public void setLossesAway(Integer lossesAway) {
        this.lossesAway = lossesAway;
    }

    public Integer getDrawsHome() {
        return drawsHome;
    }

    public void setDrawsHome(Integer drawsHome) {
        this.drawsHome = drawsHome;
    }

    public Integer getDrawsAway() {
        return drawsAway;
    }

    public void setDrawsAway(Integer drawsAway) {
        this.drawsAway = drawsAway;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }


}
