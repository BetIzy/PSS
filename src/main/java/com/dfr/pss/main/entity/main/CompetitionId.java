package com.dfr.pss.main.entity.main;

import java.io.Serializable;

public class CompetitionId implements Serializable {

    private String name;
    private String season;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }
}
