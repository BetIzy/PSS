package com.dfr.pss.main.entity.main;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.util.Date;

@Entity
@IdClass(EventId.class)
@Table(name = "main_event")
public class Event {

    @Id
    @Column(name = "event_team_home_name", nullable = false, length = 45)
    private String teamHomeName;

    @Id
    @Column(name = "event_team_away_name", nullable = false, length = 45)
    private String teamAwayName;

    @Id
    @Column(name = "event_date", nullable = false)
    private Date date;

    @ManyToOne(fetch=FetchType.LAZY)
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "event_team_home_name", insertable = false, updatable = false)
    private Team teamHomeEntity;


    @ManyToOne(fetch=FetchType.LAZY)
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "event_team_away_name", insertable = false, updatable = false)
    private Team teamAwayEntity;

    @Column(name = "event_goals_home", length = 11)
    private Integer goalsHome;

    @Column(name = "event_goals_away", length = 11)
    private Integer goalsAway;

    @Column(name = "event_goals_home_ht", length = 11)
    private Integer goalsHomeHt;

    @Column(name = "event_goals_away_ht", length = 11)
    private Integer goalsAwayHt;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name = "event_competition_name", insertable = false, updatable = false),
            @JoinColumn(name = "event_competition_season", insertable = false, updatable = false)
    })
    private Competition competition;

    @Column(name = "event_competition_name", nullable = false, length = 45)
    private String competitionName;

    @Column(name = "event_competition_season", nullable = false, length = 45)
    private String competitionSeason;

    @Column(name = "event_odd_home")
    private Double oddHome;

    @Column(name = "event_odd_draw")
    private Double oddDraw;

    @Column(name = "event_odd_away")
    private Double oddAway;

    public Double getOddHome() {
        return oddHome;
    }

    public void setOddHome(Double oddHome) {
        this.oddHome = oddHome;
    }

    public Double getOddDraw() {
        return oddDraw;
    }

    public void setOddDraw(Double oddDraw) {
        this.oddDraw = oddDraw;
    }

    public Double getOddAway() {
        return oddAway;
    }

    public void setOddAway(Double oddAway) {
        this.oddAway = oddAway;
    }

    public String getTeamHomeName() {
        return teamHomeName;
    }

    public void setTeamHomeName(String teamHomeName) {
        this.teamHomeName = teamHomeName;
    }

    public String getTeamAwayName() {
        return teamAwayName;
    }

    public void setTeamAwayName(String teamAwayName) {
        this.teamAwayName = teamAwayName;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Team getTeamHomeEntity() {
        return teamHomeEntity;
    }

    public void setTeamHomeEntity(Team teamHomeEntity) {
        this.teamHomeEntity = teamHomeEntity;
    }

    public Team getTeamAwayEntity() {
        return teamAwayEntity;
    }

    public void setTeamAwayEntity(Team teamAwayEntity) {
        this.teamAwayEntity = teamAwayEntity;
    }

    public Integer getGoalsHome() {
        return goalsHome;
    }

    public void setGoalsHome(Integer goalsHome) {
        this.goalsHome = goalsHome;
    }

    public Integer getGoalsAway() {
        return goalsAway;
    }

    public void setGoalsAway(Integer goalsAway) {
        this.goalsAway = goalsAway;
    }

    public Integer getGoalsHomeHt() {
        return goalsHomeHt;
    }

    public void setGoalsHomeHt(Integer goalsHomeHt) {
        this.goalsHomeHt = goalsHomeHt;
    }

    public Integer getGoalsAwayHt() {
        return goalsAwayHt;
    }

    public void setGoalsAwayHt(Integer goalsAwayHt) {
        this.goalsAwayHt = goalsAwayHt;
    }

    public Competition getCompetition() {
        return competition;
    }

    public void setCompetition(Competition competition) {
        this.competition = competition;
    }

    public String getCompetitionName() {
        return competitionName;
    }

    public void setCompetitionName(String competitionName) {
        this.competitionName = competitionName;
    }

    public String getCompetitionSeason() {
        return competitionSeason;
    }

    public void setCompetitionSeason(String competitionSeason) {
        this.competitionSeason = competitionSeason;
    }
}
