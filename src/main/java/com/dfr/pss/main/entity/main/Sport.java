package com.dfr.pss.main.entity.main;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="main_sport")
public class Sport {

    @Id
    @Column(unique = true, name = "sport_id", nullable = false, length = 11)
    private Integer id;

    @Column(name = "sport_name", nullable = false, length = 45)
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
