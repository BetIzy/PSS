package com.dfr.pss.main.entity.main;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="main_team")
public class Team {

    @Id
    @Column(unique = true, name = "team_name", nullable = false, length = 45)
    private String name;

    @Column(name = "team_short_name", nullable = false, length = 45)
    private String shortName;

    @OneToMany(mappedBy = "teamEntity")
    private List<Statistic> statistics;

    public List<Statistic> getStatistics() {
        return statistics;
    }

    public void setStatistics(List<Statistic> statistics) {
        this.statistics = statistics;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }
}
